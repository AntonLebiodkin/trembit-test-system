import {Component, ElementRef, HostListener, OnDestroy, OnInit} from '@angular/core';
import { Filter } from '../../models/filter.model';
import {Subscription} from 'rxjs/Subscription';
import {FilteringByTagsService} from '../../services/filtering-by-tags.service';
import {TranslateService} from 'ng2-translate';

@Component({
  selector: 'app-filter-box',
  templateUrl: './filter-box.component.html',
  styleUrls: ['./filter-box.component.scss']
})
export class FilterBoxComponent implements OnInit, OnDestroy {
  filters: Filter[];
  filtersSubscription: Subscription;
  dropdownClass = 'hidden-class';
  clearFiltersText = 'filters.clear';
  filtersText = 'filters.filtersText';

  constructor(private elRef: ElementRef,
              private translateService: TranslateService,
              private testFiltersService: FilteringByTagsService) { }

  ngOnInit() {
    this.filtersSubscription = this.testFiltersService.filtersChanged
      .subscribe(
        (filters) => {
          this.filters = filters;
        }
      );
    this.translateService.get(this.clearFiltersText).subscribe((translation) => {
      this.clearFiltersText = translation;
    });
    this.translateService.get(this.filtersText).subscribe((translation) => {
      this.filtersText = translation;
    });
  }

  ngOnDestroy() {
    if (this.filtersSubscription) {
      this.filtersSubscription.unsubscribe();
    }
  }

  toggleDropdown() {
    this.dropdownClass = this.dropdownClass === 'hidden-class' ? 'shown-class' : 'hidden-class';
  }

  @HostListener('document:click') onClick() {
    if (this.dropdownClass !== 'hidden-class') {
      let parent = event.srcElement;
      while (parent = parent.parentElement) {
        if (parent === this.elRef.nativeElement) {
          break;
        }
      }
      if (!parent) {
        this.dropdownClass = 'hidden-class';
      }
    }
  };

  anyActive() {
    return this.testFiltersService.anyActiveFilters();
  }

  changeFilter(index: number) {
    this.testFiltersService.changeFilter(index);
  }

  clearFilters() {
    this.testFiltersService.clearFilters();
    this.dropdownClass = 'hidden-class';
  }
}
