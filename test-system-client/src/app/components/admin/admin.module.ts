import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { adminRoutes } from './admin.routing';
import { Ng2DragDropModule } from 'ng2-drag-drop';


import { AdminHomeComponent } from './admin-home/admin.home.component';
import { AdminEditTestComponent } from './admin-edit-test/admin.edit-test.component';
import { AdminTestComponent } from './admin-test/admin.test.component';
import { AdminEditQuestionComponent } from './admin-edit-question/admin.edit-question.component';
import { AdminEditUserComponent } from './admin-edit-user/admin-edit-user.component';
import { AdminQuestionComponent } from './admin-questions/admin-questions.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { AdminTestsComponent } from './admin-tests/admin-tests.component';
import { AdminOneTimeTestsComponent } from './admin-one-time-tests/admin-one-time-tests.component';
import { AdminOneTimeTestComponent } from './admin-one-time-test/admin-one-time-test.component';
import { AdminTagsComponent } from './admin-tags/admin-tags.component';
import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';


@NgModule ({
  imports: [
    SharedModule,
    RouterModule.forChild(adminRoutes),
    Ng2DragDropModule,
    FormsModule,
    ReactiveFormsModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot()
  ],
  declarations: [
    AdminHomeComponent,
    AdminEditTestComponent,
    AdminTestComponent,
    AdminEditQuestionComponent,
    AdminEditUserComponent,
    AdminQuestionComponent,
    AdminUsersComponent,
    AdminTestsComponent,
    AdminOneTimeTestsComponent,
    AdminOneTimeTestComponent,
    AdminTagsComponent
  ]
})

export class AdminModule { }
