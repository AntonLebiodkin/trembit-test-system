import {Component, OnInit, OnDestroy} from '@angular/core';
import { TestService } from '../../../services/test.service';
import { Test } from '../../../models/test.model';
import { AlertService } from '../../../services/alert.service';
import { ActivatedRoute} from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import {OneTimeTest} from '../../../models/one-time-test.model';
import {OneTimeTestService} from '../../../services/one-time-test.service';
import {AppConfig} from '../../../app.config';
import {Tag} from '../../../models/tag.model';
import {TagService} from '../../../services/tag.service';

@Component({
  selector: 'app-admin-edit-test',
  templateUrl: './admin.test.component.html',
  styleUrls: ['./admin.test.component.scss']
})
export class AdminTestComponent implements OnInit, OnDestroy {
  private id: string;
  private subscription: Subscription;
  public oneTimeTest: OneTimeTest;

  public test: Test;
  public tags: Tag[];

  // filtering by tags vars
  public questions = [];
  // filtering by tags end vars

  // One time test block
  public oneTimeTestToClipboard = '';

  constructor(private testService: TestService,
              private alertService: AlertService,
              private activateRoute: ActivatedRoute,
              private oneTimeTestService: OneTimeTestService,
              private tagService: TagService) {
    this.subscription = activateRoute.params
      .subscribe(
        (params) => {
          this.id = params['id'];
        });
  }

  updateTestAccessType(test) {
    test.accessType = (test.accessType === 'public') ? 'private' : 'public';
    this.updateTest();
  }

  ngOnInit() {
    this.testService.getById(this.id)
      .subscribe(
        test => {
          this.test = test;
          this.getAvailableTags();
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  createOneTimeTest() {
    this.oneTimeTest = new OneTimeTest();
    this.oneTimeTest.test = this.test;
    this.oneTimeTestService.create(this.oneTimeTest).subscribe(
      (newOneTimeTest) => {
        this.oneTimeTest = newOneTimeTest;
        this.oneTimeTestToClipboard = AppConfig.frontUrl + '/one-time-test/' + newOneTimeTest._id;
        this.alertService.success('oneTimeTestUpdated');
      },
      (error) => {
        this.alertService.error(error._body);
      }
    );
  }

  addTagToTest(tag: Tag, index: number) {
    this.test.tags.push(tag);
    this.tags.splice(index, 1);
    this.updateTest();
  }

  removeTagFromTest(tag: Tag, index: number) {
    this.test.tags.splice(index, 1);
    this.tags.push(tag);
    this.updateTest();
  }

  getAvailableTags() {
    this.tagService.getAll().subscribe(
      (tags) => {
        for (let i = 0; i < this.test.tags.length; i++) {
          for (let j = 0; j < tags.length; j++) {
            if (tags[j]._id === this.test.tags[i]._id) {
              tags.splice(j--, 1);
            }
          }
        }
        this.tags = tags;
      },
      (error) => {
        this.alertService.error(error._body);
      }
    );
  }

  private updateTest() {
    this.testService.update(this.test).subscribe(
      () => {
        this.alertService.success('testUpdated');
      },
      (error) => {
        this.alertService.error(error._body);
      }
    );
  }
}
