import {Component, OnDestroy, OnInit} from '@angular/core';
import {OneTimeTestService} from '../../../services/one-time-test.service';
import {AlertService} from '../../../services/alert.service';
import {FilteringByTagsService} from '../../../services/filtering-by-tags.service';
import {Subscription} from 'rxjs/Subscription';
import {SortingService} from '../sorting.service';

@Component({
  selector: 'app-one-time-tests',
  templateUrl: './admin-one-time-tests.component.html',
  styleUrls: ['./admin-one-time-tests.component.scss']
})
export class AdminOneTimeTestsComponent implements OnInit, OnDestroy {
  oneTimeTests: any[];
  // filter vars block
  filtersChangedSubscription: Subscription;
  filteredOneTimeTests = [];
  // filter vars block end
  public sortedBy = {prop: ''};

  constructor(private oneTimeTestService: OneTimeTestService,
              private filteringByTagsService: FilteringByTagsService,
              private sortingService: SortingService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.oneTimeTestService.getAll()
      .subscribe((oneTimeTests) => {
        this.oneTimeTests = oneTimeTests;
        this.fillOneTimeTestsTags();
        this.filteringByTagsService.initTagFilters(this.oneTimeTests, this.filteredOneTimeTests);
      });
    this.filtersChangedSubscription = this.filteringByTagsService.filtersChanged
      .subscribe(
        (filters) => {
          this.filteredOneTimeTests = this.filteringByTagsService.getFilteredArray(this.oneTimeTests, this.filteredOneTimeTests);
        }
      );
  }

  sortByProperty(prop, sortedBy, subprop = null) {
    this.sortingService.sortData([this.filteredOneTimeTests, this.oneTimeTests], prop, sortedBy, subprop);
  }

  ngOnDestroy() {
    if (this.filtersChangedSubscription) {
      this.filtersChangedSubscription.unsubscribe();
    }
  }

  fillOneTimeTestsTags() {
    for (let i = 0; i < this.oneTimeTests.length; i++) {
      this.oneTimeTests[i].tags = this.oneTimeTests[i].test.tags;
    }
  }

  deleteOneTimeTest(event, oneTimeTest) {
    event.stopPropagation();
    this.oneTimeTestService.delete(oneTimeTest._id)
      .subscribe(
        data => {
          const index = this.oneTimeTests.findIndex(
            (innerOneTimeTest) => {
              return innerOneTimeTest._id === oneTimeTest._id;
            }
          );
          this.oneTimeTests.splice(index, 1);
          const filteredIndex = this.filteredOneTimeTests.findIndex(
            (innerOneTimeTest) => {
              return innerOneTimeTest._id === oneTimeTest._id;
            }
          );
          this.filteredOneTimeTests.splice(filteredIndex, 1);
          this.filteringByTagsService.recalculateFilters(this.oneTimeTests);
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }
}
