import {Component, OnInit, OnDestroy, DoCheck} from '@angular/core';
import { QuestionService } from '../../../services/question.service';
import { AlertService } from '../../../services/alert.service';
import {ActivatedRoute, Router} from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { FormGroup, Validators, FormBuilder, FormArray } from '@angular/forms';
import {Question} from '../../../models/question.model';
import {Tag} from '../../../models/tag.model';
import {TagService} from '../../../services/tag.service';


@Component({
  selector: 'app-admin-create-question',
  templateUrl: './admin.edit-question.component.html',
  styleUrls: ['./admin.edit-question.component.scss'],

})
export class AdminEditQuestionComponent implements OnInit, OnDestroy, DoCheck {

  public questionForm: FormGroup;
  public tags: Tag[];
  question: Question;

  private id: string;
  private subscription: Subscription;

  // popup variables
  popupMode = 'previewQuestion';
  showPreview = false;
  // end of popup variables

  public froalaOptions = {
    paragraphFormat: {
      p: 'Normal',
      code: 'Code',
      h1: 'Heading 1',
      h2: 'Heading 2',
      h3: 'Heading 3',
      h4: 'Heading 4'
    },
    tabSpaces: 2
  };

  constructor(private fb: FormBuilder,
              private questionService: QuestionService,
              private alertService: AlertService,
              private router: Router,
              private tagService: TagService,
              private activatedRoute: ActivatedRoute) {
    this.subscription = activatedRoute.params.subscribe(params => this.id = params['id']);
  }


  ngOnInit() {

    this.questionForm = this.fb.group({
      question: ['', Validators.required],
      tags: [[]],
      questionType: ['checkbox', Validators.required],
      answers: this.fb.array([])
    });

    if (this.id) {
      this.questionService.getById(this.id)
        .subscribe(
          data => {
            if (data) {
              this.questionForm = this.fb.group({
                question: [data.question, Validators.required],
                questionType: ['checkbox', Validators.required],
                answers: this.fb.array([]),
                tags: [data.tags]
              });
              data.options.forEach((element, index) => {
                let rightAnswer = false;
                data.correctAnswers.forEach(el => {
                  if (el === index) {
                    this.addAnswer(element, true);
                    rightAnswer = true;
                  }
                });
                if (!rightAnswer) {
                  this.addAnswer(element, false);
                }
              });
            }
          },
          error => {
            this.alertService.error(error._body);
          }
        );
    }

    this.addAnswer('', false);

    this.tagService.getAll().subscribe(
      (tags) => {
        this.tags = tags;
      },
      (error) => {
        this.alertService.error(error._body);
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  initAnswers(answer: string, rightAnswer: boolean) {
    return this.fb.group({
      answer: [answer, Validators.required],
      rightAnswer: [rightAnswer]
    });
  }

  addAnswer(answer: string, rightAnswer: boolean) {
    const control = <FormArray>this.questionForm.controls['answers'];
    control.push(this.initAnswers(answer, rightAnswer));
  }

  removeAnswer(i: number) {
    const control = <FormArray>this.questionForm.controls['answers'];
    control.removeAt(i);
  }

  onSubmit(form) {
    this.question.options = [];
    this.question['correctAnswers'] = [];
    form.value.answers.forEach((el, index) => {
      this.question.options.push(el.answer);
      if (el.rightAnswer) {
        this.question['correctAnswers'].push(index);
      }
    });

    if (this.id) {
      this.question._id = this.id;
      this.questionService.update(this.question)
        .subscribe(
          data => {
            this.router.navigate(['../'], {relativeTo: this.activatedRoute});
          },
          error => {
            this.alertService.error(error._body);
          }
        );
    } else {
      this.questionService.create(this.question)
        .subscribe(
          data => {
            this.router.navigate(['../'], {relativeTo: this.activatedRoute});
          },
          error => {
            this.alertService.error(error._body);
          }
        );
    }
  }

  ngDoCheck() {
    this.question = this.questionForm.value;
    if (this.question.tags && this.question.tags.length && this.tags) {
      for (let i = 0; i < this.question.tags.length; i++) {
        for (let j = 0; j < this.tags.length; j++) {
          if (this.tags[j]._id === this.question.tags[i]._id) {
            this.tags.splice(j--, 1);
          }
        }
      }
    }
  }

  previewQuestion() {
    this.showPreview = true;
  }

  hidePopup() {
    this.showPreview = false;
  }

  addTagToQuestion(tag: Tag, index: number) {
    if (!this.question.tags) {
      this.question.tags = [];
    }
    this.question.tags.push(tag);
    this.tags.splice(index, 1);
  }

  removeTagFromQuestion(tag: Tag, index: number) {
    this.question.tags.splice(index, 1);
    this.tags.push(tag);
  }
}
