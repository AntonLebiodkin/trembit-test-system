import { Injectable } from '@angular/core';

@Injectable()
export class SortingService {

  constructor() { }

  public sortData(dataArray, prop, sortedBy, subprop) {
    dataArray.forEach((data) => {
      if (prop === sortedBy.prop) {
        return data.reverse();
      }
      data.sort((a, b) => {
        let prop1 = a[prop];
        let prop2 = b[prop];
        if (subprop && prop1 && prop2) {
          prop1 = prop1[subprop];
          prop2 = prop2[subprop];
        }
        if (!prop2 || prop1 > prop2) {
          return 1;
        }
        if (!prop1 || prop1 < prop2) {
          return -1;
        }
        return 0;
      });
    });
    sortedBy.prop = prop;
  }
}
