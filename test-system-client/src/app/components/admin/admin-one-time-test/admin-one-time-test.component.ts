import { Component, OnInit } from '@angular/core';
import {OneTimeTest} from '../../../models/one-time-test.model';
import {AlertService} from '../../../services/alert.service';
import {OneTimeTestService} from '../../../services/one-time-test.service';
import {ActivatedRoute, Router} from '@angular/router';
import {AppConfig} from '../../../app.config';

@Component({
  selector: 'app-one-time-test-view',
  templateUrl: './admin-one-time-test.component.html',
  styleUrls: ['./admin-one-time-test.component.scss']
})
export class AdminOneTimeTestComponent implements OnInit {
  public oneTimeTest: OneTimeTest;
  public oneTimeTestToClipboard = '';
  public resultToClipboard = '';

  constructor(private alertService: AlertService,
              private activatedRoute: ActivatedRoute,
              private router: Router,
              private oneTimeTestService: OneTimeTestService) { }

  ngOnInit() {

    const id = this.activatedRoute.snapshot.params['id'];
    this.oneTimeTestService.getById(id)
      .subscribe(
        data => {
          this.oneTimeTestToClipboard = AppConfig.frontUrl + '/one-time-test/' + data._id;
          this.oneTimeTest = data;
          if (this.oneTimeTest.result) {
            this.resultToClipboard = AppConfig.frontUrl + '/result/' + this.oneTimeTest.result;
          }
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }

  deleteOneTimeTest() {
    this.oneTimeTestService.delete(this.oneTimeTest._id)
      .subscribe(
        data => {
          this.router.navigate(['../'], {relativeTo: this.activatedRoute});
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }

  setAssignee() {
    if (!this.oneTimeTest.assignee || this.oneTimeTest.assignee === '') {
      return;
    }
    this.oneTimeTestService.setAssignee(this.oneTimeTest)
      .subscribe(
        (oneTimeTest: OneTimeTest) => {
          this.oneTimeTest = oneTimeTest;
          this.alertService.success('assigneeSet');
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }

  copyLinkToClipboard() {
    this.alertService.success('copiedToClipboard');
  }

}
