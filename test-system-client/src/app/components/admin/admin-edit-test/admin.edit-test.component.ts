import { Component, OnDestroy, OnInit } from '@angular/core';
import { TestService } from '../../../services/test.service';
import { Test } from '../../../models/test.model';
import { AlertService } from '../../../services/alert.service';
import { QuestionService } from '../../../services/question.service';
import { Location } from '@angular/common';
import { TagService} from '../../../services/tag.service';
import { Tag } from '../../../models/tag.model';
import { FilteringByTagsService } from '../../../services/filtering-by-tags.service';
import { Subscription } from 'rxjs/Subscription';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-admin-addtests',
  templateUrl: './admin.edit-test.component.html',
  styleUrls: ['./admin.edit-test.component.scss'],
})
export class AdminEditTestComponent implements OnInit, OnDestroy {
  public tests: Test[];
  public test: Test;
  public tags: Tag[];

  public id: string;
  private subscription: Subscription;

  public nonFilteredQuestions = [];
  // filtering by tags vars
  public questions = [];
  private questionsChangedSubscription: Subscription;
  // filtering by tags end vars

  public droppedQuestions: any[] = [];

  constructor(private testService: TestService,
              private filteringByTagsService: FilteringByTagsService,
              private tagService: TagService,
              private alertService: AlertService,
              private questionService: QuestionService,
              private _location: Location,
              private activatedRoute: ActivatedRoute) {
    this.subscription = activatedRoute.params.subscribe(params => this.id = params['id']);
  }

  goBack() {
      this._location.back();
  }

  delItem(item: any) {
    const index = this.nonFilteredQuestions.findIndex((nonFilteredQuestion) => {
      if (nonFilteredQuestion._id === item._id) {
        return true;
      }
    });
    this.nonFilteredQuestions[index].disabled = false;
    this.droppedQuestions.splice(this.droppedQuestions.indexOf(item), 1);
  }

  onItemDrop(e: any) {
    if (e.dragData) {
      this.droppedQuestions.push(e.dragData);
      this.questions[this.questions.indexOf(e.dragData)].disabled = true;
    }
  }

  saveTest() {
    for (let i = 0; i < this.droppedQuestions.length; i++) {
      this.test.questions[i] = this.droppedQuestions[i]._id;
    }
    if (this.test._id) {
      this.updateTest();
      return;
    }
    this.testService.create(this.test)
      .subscribe(
        () => {
          this.goBack();
        },
          error => {
          this.alertService.error(error._body);
        }
      );
  }

  updateTest() {
    this.testService.update(this.test)
      .subscribe(
        () => {
          this.goBack();
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }

  ngOnInit() {
    this.test = new Test();
    this.test.description = 'No description given';
    this.test.tags = [];
    this.test.topic = 'Test topic';
    this.test.questions = [];
    this.test.accessType = 'public';

    if (this.id) {
      this.testService.getById(this.id)
        .subscribe(
          test => {
            this.test = test;
            this.droppedQuestions = test.questions;
          },
          error => {
            this.alertService.error(error._body);
          }
        );
    }

    this.questionService.getAll()
      .subscribe(
        questions => {
          this.nonFilteredQuestions = questions;
          this.test.questions.forEach((question) => {
            const index = this.nonFilteredQuestions.findIndex((filteredQuestion) => {
              if (filteredQuestion._id === question._id) {
                return true;
              }
            });
            this.nonFilteredQuestions[index].disabled = true;
          });
          this.filteringByTagsService.initTagFilters(this.nonFilteredQuestions, this.questions);
        },
        error => {
          this.alertService.error(error._body);
        }
      );
    this.questionsChangedSubscription = this.filteringByTagsService.filtersChanged
      .subscribe(
        (filters) => {
          this.questions = this.filteringByTagsService.getFilteredArray(this.nonFilteredQuestions, this.questions);
        }
      );
    this.getAvailableTags();
  }

  ngOnDestroy() {
    if (this.questionsChangedSubscription) {
      this.questionsChangedSubscription.unsubscribe();
    }
    this.subscription.unsubscribe();
  }

  getAvailableTags() {
    this.tagService.getAll().subscribe(
      (tags) => {
        for (let i = 0; i < this.test.tags.length; i++) {
          for (let j = 0; j < tags.length; j++) {
            if (tags[j]._id === this.test.tags[i]._id) {
              tags.splice(j--, 1);
            }
          }
        }
        this.tags = tags;
      },
      (error) => {
        this.alertService.error(error._body);
      }
    );
  }

  addTagToTest(tag: Tag, index: number) {
    this.test.tags.push(tag);
    this.tags.splice(index, 1);
  }

  removeTagFromTest(tag: Tag, index: number) {
    this.test.tags.splice(index, 1);
    this.tags.push(tag);
  }
}
