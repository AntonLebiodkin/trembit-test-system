import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { UserService } from '../../../services/user.service';
import { AlertService } from '../../../services/alert.service';
import { TestService } from '../../../services/test.service';
import { Test } from '../../../models/test.model';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-edit-user.component.html',
  styleUrls: ['./admin-edit-user.component.scss'],
  providers: [Modal]
})
export class AdminEditUserComponent implements OnInit, OnDestroy {
  private id: string;
  private subscription: Subscription;
  public user: any = {completedTests: [], assignedTests: []};
  public tests: Test[];
  public testId = '';

  constructor(private userService: UserService,
              private alertService: AlertService,
              private activateRoute: ActivatedRoute,
              private testService: TestService) {
    this.subscription = activateRoute.params.subscribe(
      params => this.id = params['id']
    );
  }

  assignTest() {
    if (this.testId === '') {
      return;
    }
    this.userService.assignTest(this.user._id, this.testId)
      .subscribe(
        user => {
          this.user = user;
        }
      );
  }

  updateAdmin() {
      this.userService.toggleAdmin(this.user._id).subscribe();
  }

  isAdmin() {
    if (this.user && this.user.permissions) {
      return this.user.permissions.includes('admin');
    }
  }

  ngOnInit() {
    this.userService.getUserById(this.id)
      .subscribe(
          data => {
            this.user = data;
          },
          error => {
            this.alertService.error(error._body);
          }
        );

    this.testService.getAll()
      .subscribe(
        data => {
          this.tests = data;
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }

  unassignTest(id) {
    this.userService.unassignTest(this.user._id, id)
      .subscribe(
        user => {
          this.user = user;
        }
      );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
