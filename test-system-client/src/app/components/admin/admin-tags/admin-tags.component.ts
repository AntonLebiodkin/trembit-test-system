import {Component, ElementRef, OnInit} from '@angular/core';
import {Tag} from '../../../models/tag.model';
import {AlertService} from '../../../services/alert.service';
import {TagService} from '../../../services/tag.service';

@Component({
  selector: 'app-admin-tags',
  templateUrl: './admin-tags.component.html',
  styleUrls: ['./admin-tags.component.scss']
})
export class AdminTagsComponent implements OnInit {
  public tags: Tag[];
  public newTagTitle = '';

  constructor(private tagService: TagService,
              private elementRef: ElementRef,
              private alertService: AlertService) { }

  ngOnInit() {
    this.tagService.getAll()
      .subscribe(
        (tags) => {
          this.tags = tags;
        },
        (error) => {
          this.alertService.error(error._body);
        }
      );
  }

  deleteTag(tag, index) {
    this.tagService._delete(tag._id)
      .subscribe(
        () => {
          this.tags.splice(index, 1);
        },
        (error) => {
          this.alertService.error(error._body);
        }
      );
  }

  addTag() {
    const tag = new Tag();
    tag.title = this.newTagTitle;
    this.tagService.create(tag).subscribe(
      (newTag: Tag) => {
        this.newTagTitle = '';
        this.tags.push(newTag);
      },
      (error) => {
        this.alertService.error(error._body);
      }
    );
  }

  editTag(tag: Tag, index: number) {
    this.tagService.update(tag).subscribe(
      (editedTag: Tag) => {
        this.tags[index] = editedTag;
      },
      (error) => {
        this.alertService.error(error._body);
      }
    );
  }

  dirtyInput(index: number) {
    const inputElement = this.elementRef.nativeElement.querySelector('#i' + index);
    if (inputElement) {
      return inputElement.classList.contains('ng-dirty') && inputElement.classList.contains('ng-valid');
    }
    return true;
  }

  cantCreateNew() {
    return this.newTagTitle === '';
  }

}
