import {Component, OnDestroy, OnInit} from '@angular/core';
import { AlertService } from '../../../services/alert.service';
import { QuestionService } from '../../../services/question.service';
import {Subscription} from 'rxjs/Subscription';
import {Question} from '../../../models/question.model';
import {FilteringByTagsService} from '../../../services/filtering-by-tags.service';
import {SortingService} from '../sorting.service';
import {DomSanitizer} from '@angular/platform-browser';


@Component({
  selector: 'app-view-questions',
  templateUrl: './admin-questions.component.html',
  styleUrls: ['./admin-questions.component.scss']
})
export class AdminQuestionComponent implements OnInit, OnDestroy {
  public questions: Question[];
  public sortedBy = {prop: ''};

  // filter vars block
  filtersChangedSubscription: Subscription;
  filteredQuestions = [];
  // filter vars block end

  constructor(private alertService: AlertService,
              private filteringByTagsService: FilteringByTagsService,
              private sortingService: SortingService,
              private sanitizer: DomSanitizer,
              private questionService: QuestionService) {}

  ngOnInit() {
    this.questionService.getAll()
      .subscribe(
        questions => {
          this.questions = questions;
          this.sanitizeQuestions();
          this.filteringByTagsService.initTagFilters(this.questions, this.filteredQuestions);
        },
        error => {
          this.alertService.error(error._body);
        }
      );
    this.filtersChangedSubscription = this.filteringByTagsService.filtersChanged
      .subscribe(
        (filters) => {
          this.filteredQuestions = this.filteringByTagsService.getFilteredArray(this.questions, this.filteredQuestions);
        }
      );
  }

  sanitizeQuestions() {
    this.questions.forEach((question) => {
      question['sanitizedQuestion'] = this.sanitizer.bypassSecurityTrustHtml(question.question);
    });
  }

  ngOnDestroy() {
    if (this.filtersChangedSubscription) {
      this.filtersChangedSubscription.unsubscribe();
    }
  }

  sortByProperty(prop, sortedBy, subprop = null) {
    this.sortingService.sortData([this.filteredQuestions, this.questions], prop, sortedBy, subprop);
  }

  deleteQuestion($event, question) {
    $event.stopPropagation();
    this.questionService.delete(question._id)
      .subscribe(
        (data) => {
          const index = this.questions.findIndex(
            (innerQuestion) => {
              return innerQuestion._id === question._id;
            }
          );
          this.questions.splice(index, 1);
          const filteredIndex = this.filteredQuestions.findIndex(
            (innerQuestion) => {
              return innerQuestion._id === question._id;
            }
          );
          this.filteredQuestions.splice(filteredIndex, 1);
          this.filteringByTagsService.recalculateFilters(this.questions);
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }
}
