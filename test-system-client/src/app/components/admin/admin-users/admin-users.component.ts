import { Component, OnInit } from '@angular/core';
import { UserService } from "../../../services/user.service";
import { AlertService } from "../../../services/alert.service";
import { User } from "../../../models/user.model";
import {SortingService} from '../sorting.service';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit {
  public users: User[];
  public sortedBy = {prop: ''};

  constructor(
    private userService: UserService,
    private sortingService: SortingService,
    private alertService: AlertService) {}

  sortByProperty(prop, sortedBy, subprop = null) {
    this.sortingService.sortData([this.users], prop, sortedBy, subprop);
  }

  ngOnInit() {
    this.userService.getAll()
      .subscribe(
        users => {
          this.users = users;
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }
}
