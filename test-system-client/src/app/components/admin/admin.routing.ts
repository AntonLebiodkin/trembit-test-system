import {Routes} from '@angular/router';

import { AdminHomeComponent } from './admin-home/admin.home.component';
import { AdminEditTestComponent } from './admin-edit-test/admin.edit-test.component';
import { AdminEditUserComponent } from './admin-edit-user/admin-edit-user.component';
import { AdminTestComponent } from './admin-test/admin.test.component';
import { AdminEditQuestionComponent } from './admin-edit-question/admin.edit-question.component';
import { AdminQuestionComponent } from './admin-questions/admin-questions.component';
import {AdminUsersComponent} from './admin-users/admin-users.component';
import {AdminGuard} from '../../guards/admin.guard';
import {AdminTestsComponent} from 'app/components/admin/admin-tests/admin-tests.component';
import {AdminOneTimeTestsComponent} from './admin-one-time-tests/admin-one-time-tests.component';
import {AdminOneTimeTestComponent} from './admin-one-time-test/admin-one-time-test.component';
import {AdminTagsComponent} from './admin-tags/admin-tags.component';

export const adminRoutes: Routes = [
  { path: 'admin', component: AdminHomeComponent, canActivate: [AdminGuard], children: [
    { path: 'questions', component: AdminQuestionComponent },
    { path: 'questions/new', component: AdminEditQuestionComponent },
    { path: 'questions/:id', component: AdminEditQuestionComponent },

    { path: 'users', component: AdminUsersComponent },
    { path: 'users/:id', component: AdminEditUserComponent },

    { path: 'tests', component: AdminTestsComponent },
    { path: 'tests/new', component: AdminEditTestComponent },
    { path: 'tests/:id/edit', component: AdminEditTestComponent },
    { path: 'tests/:id', component: AdminTestComponent },

    { path: 'one-time-tests', component: AdminOneTimeTestsComponent },
    { path: 'one-time-tests/:id', component: AdminOneTimeTestComponent },

    { path: 'tags', component: AdminTagsComponent },

    { path: '', redirectTo: 'users', pathMatch: 'full' }
  ]}
];
