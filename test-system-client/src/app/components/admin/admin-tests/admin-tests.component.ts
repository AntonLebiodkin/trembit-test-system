import {Component, OnDestroy, OnInit} from '@angular/core';
import {Test} from '../../../models/test.model';
import {TestService} from '../../../services/test.service';
import {AlertService} from '../../../services/alert.service';
import {Subscription} from 'rxjs/Subscription';
import {FilteringByTagsService} from '../../../services/filtering-by-tags.service';
import {SortingService} from '../sorting.service';

@Component({
  selector: 'app-admin-tests',
  templateUrl: './admin-tests.component.html',
  styleUrls: ['./admin-tests.component.scss']
})
export class AdminTestsComponent implements OnInit, OnDestroy {
  public tests: Test[];
  public sortedBy = {prop: ''};

  // filter vars block
  filtersChangedSubscription: Subscription;
  filteredTests = [];
  // filter vars block end

  constructor(private testService: TestService,
              private filteringByTagsService: FilteringByTagsService,
              private sortingService: SortingService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.testService.getAll()
      .subscribe(
        data => {
          this.tests = data;
          this.filteringByTagsService.initTagFilters(this.tests, this.filteredTests);
        },
        error => {
          this.alertService.error(error._body);
        }
      );
    this.filtersChangedSubscription = this.filteringByTagsService.filtersChanged
      .subscribe(
        (filters) => {
          this.filteredTests = this.filteringByTagsService.getFilteredArray(this.tests, this.filteredTests);
        }
      );
  }

  ngOnDestroy() {
    if (this.filtersChangedSubscription) {
      this.filtersChangedSubscription.unsubscribe();
    }
  }

  sortByProperty(prop, sortedBy, subprop = null) {
    this.sortingService.sortData([this.filteredTests, this.tests], prop, sortedBy, subprop);
  }

  deleteTest(event, test) {
    event.stopPropagation();
    this.testService.delete(test._id)
      .subscribe(
        (data) => {
          const index = this.tests.findIndex(
            (innerTest) => {
              return innerTest._id === test._id;
            }
          );
          this.tests.splice(index, 1);
          const filteredIndex = this.filteredTests.findIndex(
            (innerTest) => {
              return innerTest._id === test._id;
            }
          );
          this.filteredTests.splice(filteredIndex, 1);
          this.filteringByTagsService.recalculateFilters(this.tests);
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }
}
