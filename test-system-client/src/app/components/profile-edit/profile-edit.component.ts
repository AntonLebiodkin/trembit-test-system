import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {User} from "../../models/user.model";
import {UserService} from "../../services/user.service";
import {AlertService} from "../../services/alert.service";
import {Router} from "@angular/router";
import {TranslateService} from 'ng2-translate';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {
  imgUrl: string;
  user: User;
  userForm: FormGroup;
  constructor(private userService: UserService,
              private router: Router,
              private alertService: AlertService,
              private translate: TranslateService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.userService.getCurrent().subscribe(
      (user) => {
        this.user = user;
        this.initForm();
        this.imgUrl = this.user.imageUrl || 'assets/avatar_2x.png';
      }
    );
  }

  //image preview when user selects one
  changeListener(event) {
    if (event.target.files[0]) {
      const reader = new FileReader();
      reader.onload = () => {
        this.imgUrl = reader.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  /*
    TO DO
    add validator to check if email exists
   */
  initForm() {
    if (this.user.emailCredentials) {
      this.userForm = this.formBuilder.group({
        'firstName': [this.user.firstName],
        'lastName': [this.user.lastName],
        'email': [this.user.emailCredentials.email,
          [Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)]],
        'language': [this.user.language || 'en'],
        'birthDate': [this.user.birthDate],
        'imageUrl': [],
        'password': ['', this.passwordValidator],
        'confirmPassword': ['']
      }, {validator: this.matchingPasswords('password', 'confirmPassword')});
    } else {
      this.userForm = this.formBuilder.group({
        'firstName': [this.user.firstName],
        'lastName': [this.user.lastName],
        'language': [this.user.language || 'en'],
        'birthDate': [this.user.birthDate],
        'imageUrl': [],
      });
    }
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      const password = group.controls[passwordKey];
      const confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return { mismatchedPasswords: true };
      }
    };
  }

  //let user keep password field empty => unchanged
  passwordValidator(passControl: FormControl) {
    const password = passControl.value;
    if (password && password.length > 0 && password.length < 6) {
      return { shortPassword: true };
    }
  };

  onSubmit() {
    const user = new User();
    user._id = this.user._id;

    // send server only 'dirty' values
    for (const controlName in this.userForm.controls) {
      if (this.userForm.controls.hasOwnProperty(controlName)) {
        const currentControl = this.userForm.controls[controlName];
        if (currentControl.dirty && controlName !== 'confirmPassword') {
          user[controlName] = currentControl.value;
        }
      }
    }
    if (user['password'] === '') {
      delete user['password'];
    }
    let emailCreds; ;
    if (this.user.emailCredentials) {
      emailCreds = {
        password: user['password'],
        email: user['email'],
      };
    }
/* TO DO
    send pic to server side
*/
    this.userService.update(user, emailCreds).subscribe(
      (updatedUser) => {
        localStorage.setItem('currentUser', JSON.stringify(updatedUser));
        this.translate.use(updatedUser.language);
        this.alertService.success('profileUpdated', true);
        this.router.navigate(['/profile']);
      },
      (error) => {
        this.alertService.error(error._body);
      }
    );
  }
}
