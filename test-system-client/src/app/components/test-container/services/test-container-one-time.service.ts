import { Injectable } from '@angular/core';
import {OneTimeTest} from '../../../models/one-time-test.model';
import {OneTimeTestService} from '../../../services/one-time-test.service';
import {AlertService} from '../../../services/alert.service';
import {Result} from '../../../models/result.model';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class TestContainerOneTimeService {
  public oneTimeTestLoaded = new Subject<OneTimeTest>();
  private oneTimeTest: OneTimeTest;

  constructor(private oneTimeTestService: OneTimeTestService,
              private alertService: AlertService) { }

  public setStatus(status: string) {
    this.oneTimeTest.status = status;
    this.oneTimeTestService.updateStatus(this.oneTimeTest)
      .subscribe(
        (oneTimeTest) => {
          this.oneTimeTest = oneTimeTest;
          this.oneTimeTestLoaded.next(this.oneTimeTest);
        },
        (error) => {
          this.alertService.error(error._body, true);
        });
  }

  public setOneTimeTest(oneTimeTest: OneTimeTest) {
    this.oneTimeTest = oneTimeTest;
    this.getOneTimeTest();
  }

  public oneTime() {
    return this.oneTimeTest !== undefined;
  }

  public setResult(result: Result) {
    this.oneTimeTest.result = result;
    this.oneTimeTestService.setResult(this.oneTimeTest)
      .subscribe(
        (oneTimeTest) => {
          this.oneTimeTest = oneTimeTest;
          this.oneTimeTestLoaded.next(this.oneTimeTest);
        },
        (error) => {
          this.alertService.error(error._body, true);
        });
  }

  public getOneTimeTest() {
    this.oneTimeTestLoaded.next(this.oneTimeTest);
  }
}
