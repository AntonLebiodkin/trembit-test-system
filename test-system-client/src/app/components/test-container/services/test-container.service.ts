import { Injectable } from '@angular/core';
import { Test } from "../../../models/test.model";
import { TestService } from "../../../services/test.service";
import { AlertService } from "../../../services/alert.service";
import { Subject } from "rxjs/Subject";
import { Result } from "../../../models/result.model";
import { UserService } from "../../../services/user.service";
import { Router } from "@angular/router";
import {TestContainerOneTimeService} from './test-container-one-time.service';

const currentUser = JSON.parse(localStorage.getItem('currentUser'));
const tick = 1000;

@Injectable()
export class TestContainerService {
  public static started = false;
  public testLoaded = new Subject<Test>();
  public resultReceived = new Subject<Result>();
  public curQuestion = new Subject<number>();
  public userAnswersChanged = new Subject<any[]>();
  public timerTicked = new Subject<number>();
  public outOfTime = new Subject<boolean>();
  private timeLeft: number;
  private timer;

  private currentQuestion: number;
  private userAnswers = [];
  private test: Test;
  private result: Result;

  constructor(private testService: TestService,
              private userService: UserService,
              private router: Router,
              private testingOneTimeService: TestContainerOneTimeService,
              private alertService: AlertService) { }

  // get test from server if none or other is loaded and provide it
  public setTest(id: string) {
    if (!this.test || this.test._id !== id) {
      this.testService.getById(id).subscribe(
        test => {
          this.test = test;
          this.initTest();
          this.getTest();
        },
        error => {
          this.router.navigate(['/profile']);
          this.alertService.error(error._body);
        }
      );
    } else {
      this.getTest();
    }
  }

  // provide test value to all subscribers
  public getTest() {
    this.testLoaded.next(this.test);
  }

  public getTestDuration() {
    return this.test.time;
  }

  public setCurrentQuestion(index: number) {
    if (index >= 0 && index < this.test.questions.length && index !== this.currentQuestion) {
      this.currentQuestion = index;
      this.curQuestion.next(this.currentQuestion);
    }
  }

  public startTest() {
    TestContainerService.started = true;
    this.timeLeft = this.test.time;
    if (this.testingOneTimeService.oneTime()) {
      this.testingOneTimeService.setStatus('started');
    }
    this.result = new Result(currentUser, this.test, []);
    this.result.startTime = Date.now();
    // start timer, probably should move to backend
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.timer = setInterval(() => {
      this.timeLeft -= tick;
      this.timerTicked.next(this.timeLeft);
      if (this.timeLeft <= 0) {
        this.outOfTime.next(true);
        clearInterval(this.timer);
      }
    }, tick);
  }

  // set result final data and return promise with result from server
  public finishTest() {
    TestContainerService.started = false;
    let makeResultShared = false;
    clearInterval(this.timer);
    this.result.test = this.test;
    this.result.endTime = Date.now();
    this.result.userAnswers = this.userAnswers;

    // one-time-test finish block
    if (this.testingOneTimeService.oneTime()) {
      makeResultShared = true;
      this.testingOneTimeService.setStatus('finished');
    }
    // finish test on backend
    this.userService.completeTest(this.result, makeResultShared)
      .subscribe(
        (response) => {
          this.result = response.json();
          this.test = null;
          // binding one time test to result
          if (this.testingOneTimeService.oneTime()) {
            this.testingOneTimeService.setResult(this.result);
          }
          // provide result to test-question-view component to correctly redirect on time end
          this.resultReceived.next(this.result);
          if (this.timeLeft > 0) {
            this.router.navigate(['/result', this.result._id]);
          }
        }
      );
  }

  public getUserAnswers() {
    this.userAnswersChanged.next(this.userAnswers.slice());
  }

  public saveAnswer(index: number, answer: number[]) {
    this.userAnswers[index] = answer;
    this.getUserAnswers();
  }

  private initTest() {
    for (let i = 0; i < this.test.questions.length; i++) {
      this.userAnswers[i] = [];
    }
    this.getUserAnswers();
  }

}
