import { NgModule } from '@angular/core';
import {RouterModule} from "@angular/router";
import {testContainerRoutes} from "./test-container.routing";
import { TestContainerComponent } from './test-container-component/test-container.component';
import {TestQuestionViewComponent} from "./test-wrapper/test-question-view/test-question-view.component";
import {TestDescriptionComponent} from "./test-description/test-description.component";
import {TestWrapperComponent} from "./test-wrapper/test-wrapper.component";
import {TestProgressComponent} from "./test-wrapper/test-progress/test-progress.component";
import {TestTimerComponent} from "./test-wrapper/test-timer/test-timer.component";
import {TestContainerService} from "./services/test-container.service";
import {SharedModule} from "../../shared/shared.module";
import {TestContainerOneTimeService} from './services/test-container-one-time.service';
import {ReadDescriptionGuard} from './guards/read-description.guard';
import {CanDeactivateTestGuard} from './guards/canDeactivateTest.guard';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(testContainerRoutes),
    FormsModule
  ],
  declarations: [
    TestContainerComponent,
    TestQuestionViewComponent,
    TestDescriptionComponent,
    TestWrapperComponent,
    TestProgressComponent,
    TestTimerComponent
  ],
  providers: [
    TestContainerService,
    TestContainerOneTimeService,
    ReadDescriptionGuard,
    CanDeactivateTestGuard
  ]
})
export class TestContainerModule { }
