import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { TestContainerService } from "../services/test-container.service";
import {OneTimeTestService} from '../../../services/one-time-test.service';
import {TestContainerOneTimeService} from '../services/test-container-one-time.service';

@Component({
  selector: 'app-test-container',
  templateUrl: './test-container.component.html',
  styleUrls: ['./test-container.component.scss']
})
export class TestContainerComponent implements OnInit {

  constructor(private activatedRoute: ActivatedRoute,
              private oneTimeTestService: OneTimeTestService,
              private oneTimeTestingService: TestContainerOneTimeService,
              private testingService: TestContainerService) { }

  ngOnInit() {
    const oneTimeTestId = this.activatedRoute.snapshot.params['oneTimeId'];
    if (!oneTimeTestId) {
      const id = this.activatedRoute.snapshot.params['id'];
      this.testingService.setTest(id);
      return;
    }

    this.oneTimeTestService.getById(oneTimeTestId)
      .subscribe(
        (oneTimeTest) => {
          this.oneTimeTestingService.setOneTimeTest(oneTimeTest);
          this.testingService.setTest(oneTimeTest.test._id);
        }
      );
  }

}
