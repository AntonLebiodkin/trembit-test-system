import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {TestContainerService} from '../services/test-container.service';
import {AlertService} from '../../../services/alert.service';

@Injectable()
export class ReadDescriptionGuard implements CanActivate {

  constructor(private alertService: AlertService,
              private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (!TestContainerService.started) {
      this.alertService.success('readDescription', true);
      this.router.navigate([state.url, 'description']);
    }
    return TestContainerService.started;
  }
}
