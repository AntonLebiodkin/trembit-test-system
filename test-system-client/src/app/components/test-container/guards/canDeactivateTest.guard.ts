import {Injectable} from '@angular/core';
import { CanDeactivate} from '@angular/router';
import {TestContainerService} from '../services/test-container.service';
import {TestWrapperComponent} from '../test-wrapper/test-wrapper.component';

@Injectable()
export class CanDeactivateTestGuard implements CanDeactivate<TestWrapperComponent> {

  canDeactivate(target: TestWrapperComponent): any {
    if (TestContainerService.started) {
      target.showWarning = true;
      return target.canLeaveSubj;
    } else {
      return true;
    }
  }
}
