import {Routes} from "@angular/router";
import {TestDescriptionComponent} from "./test-description/test-description.component";
import {TestWrapperComponent} from "./test-wrapper/test-wrapper.component";
import {TestContainerComponent} from "./test-container-component/test-container.component";
import {ReadDescriptionGuard} from './guards/read-description.guard';
import {CanDeactivateTestGuard} from './guards/canDeactivateTest.guard';

export const testContainerRoutes: Routes = [
  { path: '', component: TestContainerComponent, children: [
    { path: 'description', component: TestDescriptionComponent },
    {
      path: '',
      component: TestWrapperComponent,
      canActivate: [ReadDescriptionGuard],
      canDeactivate: [CanDeactivateTestGuard]
    }
  ]}
]
