import {Component, OnInit, OnDestroy} from '@angular/core';
import {TestContainerService} from "../../services/test-container.service";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-test-progress',
  templateUrl: './test-progress.component.html',
  styleUrls: ['./test-progress.component.scss']
})
export class TestProgressComponent implements OnInit, OnDestroy {
  userAnswers: any[];
  currentQuestionIndex: number;
  currentQuestionSubscription: Subscription;
  userAnswersSubscription: Subscription;
  // popup data
  popupMode = 'testFinishWarning';
  showWarning = false;

  constructor(private testingService: TestContainerService) { }

  ngOnInit() {
    this.currentQuestionSubscription = this.testingService.curQuestion.subscribe(
      (curQuestion => {
        this.currentQuestionIndex = curQuestion;
      })
    );
    this.userAnswersSubscription = this.testingService.userAnswersChanged.subscribe(
      (userAnswers => {
        this.userAnswers = userAnswers;
      })
    );
    this.testingService.getUserAnswers();
  }

  onChangeQuestion(index: number) {
    this.testingService.setCurrentQuestion(index);
  }

  ngOnDestroy() {
    this.currentQuestionSubscription.unsubscribe();
  }

  onFinishTest() {
    this.showWarning = true;
    this.popupMode = 'testFinishWarning';
  }

  hidePopup() {
    this.showWarning = false;
  }

  waitForUserConfirmation() {
    this.testingService.finishTest();
  }
}
