import {Component, OnDestroy, OnInit} from '@angular/core';
import {TestContainerService} from "../../services/test-container.service";
import {Subscription} from "rxjs/Subscription";

const PI = 3.14;
const SECTORMOD = '1,0,1 ';
const RADIUS = 40;
const INITIAL_BG_TIMER_STATE = "forestgreen";
const MIDDLE_BG_TIMER_STATE = "red";
const END_BG_TIMER_STATE = "lightgrey";

@Component({
  selector: 'app-test-timer',
  templateUrl: './test-timer.component.html',
  styleUrls: ['./test-timer.component.scss']
})

export class TestTimerComponent implements OnInit, OnDestroy {
  testDuration: number;
  timeLeft: number;

  timerBgColor: string;
  sectorColor: string;
  animationName: string;

  sectorDraw: string;

  timerSubscription: Subscription;

  constructor(private testingService: TestContainerService) { }

  ngOnInit() {
    this.testDuration = this.testingService.getTestDuration();
    this.timeLeft = this.testDuration;
    this.timerSubscription = this.testingService.timerTicked.subscribe(
      (timeLeft) => {
        this.timeLeft = timeLeft;
        this.drawCircle();
      }
    );
  }

  ngOnDestroy() {
    this.timerSubscription.unsubscribe();
  }

  drawCircle() {
    let currentMaxTimer = this.testDuration;
    let currentCircleTimer = this.timeLeft;

    if (currentCircleTimer >= this.testDuration) {
      this.timerBgColor = INITIAL_BG_TIMER_STATE;
      return;
    } else if (currentCircleTimer > 60000) {
      currentCircleTimer -= 60000;
      currentMaxTimer = this.testDuration - 60000;
      this.timerBgColor = MIDDLE_BG_TIMER_STATE;
      this.sectorColor = INITIAL_BG_TIMER_STATE;
    } else {
      currentMaxTimer = 60000;
      this.timerBgColor = END_BG_TIMER_STATE;
      this.sectorColor = MIDDLE_BG_TIMER_STATE;
      if (!this.animationName) {
        this.animationName = 'ticktock';
      }
    }

    let leftAngle, rightAngle;
    if (currentCircleTimer > currentMaxTimer / 2) {
      leftAngle = PI * (currentCircleTimer * 2 - currentMaxTimer) / currentMaxTimer ;
      rightAngle = PI;
    } else {
      rightAngle = PI * currentCircleTimer * 2 / currentMaxTimer;
    }

    const rightArc = this.calculateArcXY(rightAngle);
    const leftArc = this.calculateArcXY(leftAngle, false);

    this.sectorDraw = this.drawArc(rightArc, 0);
    if (currentCircleTimer > currentMaxTimer / 2) {
      this.sectorDraw += this.drawArc(leftArc, RADIUS * 2);
    }
    this.sectorDraw += ' z';
  }

  calculateArcXY(angle: number, right = true) {
    if (right) {
      return {x: (RADIUS + RADIUS * Math.sin(angle)),
              y: (RADIUS - RADIUS * Math.cos(angle))};
    } else {
      return {x: (RADIUS - RADIUS * Math.sin(angle)),
              y: (RADIUS + RADIUS * Math.cos(angle))};
    }
  }

  drawArc(arc, startPoint) {
    return ' M' + RADIUS + ',' + RADIUS + ' L' + RADIUS + ',' + startPoint +
      ' A' + RADIUS + ',' + RADIUS + ' ' + // start drawing arc with xradius=yradius=radius
      SECTORMOD + arc['x'] + ',' + arc['y']; // arc endpoint and chosen mode to draw
  }
}
