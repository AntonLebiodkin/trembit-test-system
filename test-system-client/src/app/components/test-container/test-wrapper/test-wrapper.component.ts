import {Component, OnDestroy, OnInit} from '@angular/core';
import {Test} from '../../../models/test.model';
import {TestContainerService} from '../services/test-container.service';
import {Subscription} from 'rxjs/Subscription';
import {Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-test-wrapper',
  templateUrl: './test-wrapper.component.html',
  styleUrls: ['./test-wrapper.component.scss']
})
export class TestWrapperComponent implements OnInit, OnDestroy {
  private testSubscription: Subscription;
  test: Test;
  public canLeaveSubj = new Subject<Boolean>();
  public popupMode = 'testDeactivateWarning';
  public showWarning = false;
  canLeave = false;

  constructor(private testingService: TestContainerService) { }

  ngOnInit() {

    this.testSubscription = this.testingService.testLoaded.subscribe(
      (test: Test) => {
        this.test = test;
      }
    );

    this.testingService.getTest();
  }

  ngOnDestroy() {
    if (this.testSubscription) {
      this.testSubscription.unsubscribe();
    }
  }

  waitForUserConfirmation() {
    TestContainerService.started = false;
    this.canLeave = true;
  }

  hidePopup() {
    this.showWarning = false;
    this.canLeaveSubj.next(this.canLeave);
  }

}
