export function setSlidingClasses() {
  if (this.currentQuestionIndex - 1 === this.prevQuestionIndex) {
    if (this.activeFirstData) {
      this.data1transition = 'moveToLeft';
      this.data2transition = 'moveFromRight';
    } else {
      this.data2transition = 'moveToLeft';
      this.data1transition = 'moveFromRight';
    }
  }

  if (this.currentQuestionIndex + 1 === this.prevQuestionIndex) {
    if (this.activeFirstData) {
      this.data2transition = 'moveFromLeft';
      this.data1transition = 'moveToRight';
    } else {
      this.data1transition = 'moveFromLeft';
      this.data2transition = 'moveToRight';
    }
  }

  if (this.currentQuestionIndex - 1 > this.prevQuestionIndex) {
    if (this.activeFirstData) {
      this.data2transition = 'moveFromBottom';
      this.data1transition = 'moveToTop';
    } else {
      this.data1transition = 'moveFromBottom';
      this.data2transition = 'moveToTop';
    }
  }

  if (this.currentQuestionIndex + 1 < this.prevQuestionIndex) {
    if (this.activeFirstData) {
      this.data1transition = 'moveToBottom';
      this.data2transition = 'moveFromTop';
    } else {
      this.data2transition = 'moveToBottom';
      this.data1transition = 'moveFromTop';
    }
  }
}
