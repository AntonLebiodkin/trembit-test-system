import {Component, OnInit, Input, OnDestroy, ElementRef, AfterViewChecked, SecurityContext} from '@angular/core';
import { Question } from "../../../../models/question.model";
import { Test } from "../../../../models/test.model";
import { Subscription } from "rxjs/Subscription";
import { Router } from "@angular/router";
import { TestContainerService } from "../../services/test-container.service";
import {HighlightJsService} from 'angular2-highlight-js';
import {Result} from '../../../../models/result.model';
import {setSlidingClasses} from './sliding-animations/sliding-set-class-function';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-test-questions-view',
  templateUrl: './test-question-view.component.html',
  styleUrls: ['./test-question-view.component.scss', './sliding-animations/sliding-animations.scss']
})
export class TestQuestionViewComponent implements OnInit, OnDestroy, AfterViewChecked {
  question: Question;
  outOfTime: boolean;
  userAnswers = [];
  @Input() test: Test;
  outOfTimeSubscription: Subscription;
  receiveResultSubscription: Subscription;
  userAnswersSub: Subscription;
  currentQuestionSubscription: Subscription;
  currentUserAnswer: number[];
  result: Result;
  popupMode = 'testAutoFinishPopup';
  shouldHighlight = false;

  currentQuestionIndex: number;
  // slide variables
  prevQuestionIndex: number;

  data1index: number;
  data1: Question;
  data1transition: string;
  question1secureHtml;

  data2transition: string;
  data2: Question;
  data2index: number;
  question2secureHtml;

  activeFirstData: boolean;
  // end of slide variables

  constructor(private testingService: TestContainerService,
              private elementRef: ElementRef,
              private sanitizer: DomSanitizer,
              private higlightService: HighlightJsService,
              private router: Router) { }

  ngOnInit() {
    this.userAnswersSub = this.testingService.userAnswersChanged.subscribe(
      (userAnswers) => {
        this.userAnswers = userAnswers;
      }
    );

    this.currentQuestionSubscription = this.testingService.curQuestion.subscribe(
      (curQuestion => {
        this.shouldHighlight = true;
        this.prevQuestionIndex = this.currentQuestionIndex;
        this.currentQuestionIndex = curQuestion;
        this.question = this.test.questions[curQuestion];
        this.currentUserAnswer = this.userAnswers[curQuestion];
        this.questionChange();
      })
    );

    this.outOfTimeSubscription = this.testingService.outOfTime.subscribe(
      outOfTime => {
        this.outOfTime = outOfTime;
        if (outOfTime) {
          this.testingService.finishTest();
        }
      }
    );

    this.receiveResultSubscription = this.testingService.resultReceived.subscribe(
      result => {
        this.result = result;
      }
    );

    this.testingService.getUserAnswers();
    this.testingService.setCurrentQuestion(0);
  }

  bypassUntrustedHtml(html: string) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  /*
    Makes all given <code> blocks highlighted once per question change
    using shouldHighlight variable
    Converts block's innerHtml into innerText to display html tags, etc
    (Bad code approach)

    TODO
      Make some changes on question.question value
      (probably after creating questions via text-editor)
   */
  ngAfterViewChecked() {
    if (this.shouldHighlight) {
      this.shouldHighlight = false;
      let activeElement;
      if (this.activeFirstData) {
        activeElement = this.elementRef.nativeElement.querySelector('#data1');
      } else {
        activeElement = this.elementRef.nativeElement.querySelector('#data2');
      }
      const codeBlocks = activeElement.querySelectorAll('code');
      for (let i = 0; i < codeBlocks.length; i++) {
        codeBlocks[i].innerText = codeBlocks[i].innerHTML;
        this.higlightService.highlight(codeBlocks[i]);
      }
    }
  }

  ngOnDestroy() {
    this.outOfTimeSubscription.unsubscribe();
    this.userAnswersSub.unsubscribe();
    this.currentQuestionSubscription.unsubscribe();
  }

  chooseRadioOption(index: number) {
    this.currentUserAnswer = [index];
    this.saveAnswer(this.currentQuestionIndex, this.currentUserAnswer);
  }

  chooseMultiOption(index: number) {
    const answerIndex = this.currentUserAnswer.indexOf(index);
    if (answerIndex !== -1) {
      this.currentUserAnswer.splice(answerIndex, 1);
    } else {
      this.currentUserAnswer.push(index);
    }
    this.saveAnswer(this.currentQuestionIndex, this.currentUserAnswer);
  }

  saveAnswer(index: number, answer: number[]) {
    this.testingService.saveAnswer(index, answer);
  }

  onPrevQuestion() {
    this.testingService.setCurrentQuestion(this.currentQuestionIndex - 1);
  }

  onNextQuestion() {
    this.testingService.setCurrentQuestion(this.currentQuestionIndex + 1);
  }

  redirectOnClosePopup() {
    if (this.result) {
      this.router.navigate(['/result', this.result._id]);
    } else {
      this.router.navigate(['/profile']);
    }
  }

  firstQuestion() {
    return this.currentQuestionIndex === 0;
  }

  lastQuestion() {
    return this.currentQuestionIndex === this.test.questions.length - 1;
  }

  questionChange() {
    if (this.activeFirstData === undefined) {
      this.activeFirstData = false;
    }
    // get rid of ugly spaces before sanitizing
    this.question.question =  this.question.question.replace(/&nbsp;/g, ' ');
    if (this.activeFirstData) {
      this.data2 = this.question;
      this.data2index = this.currentQuestionIndex;
      this.question2secureHtml = this.bypassUntrustedHtml(this.data2.question);
    } else {
      this.data1 = this.question;
      this.data1index = this.currentQuestionIndex;
      this.question1secureHtml = this.bypassUntrustedHtml(this.data1.question);
    }
    // call sliding animation
    setSlidingClasses.bind(this)();

    this.activeFirstData = !this.activeFirstData;
  }

}
