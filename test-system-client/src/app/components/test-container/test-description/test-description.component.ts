import {Component, OnInit, OnDestroy, DoCheck} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {Test} from '../../../models/test.model';
import {TestContainerService} from '../services/test-container.service';
import {TestContainerOneTimeService} from '../services/test-container-one-time.service';
import {OneTimeTest} from '../../../models/one-time-test.model';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertService} from '../../../services/alert.service';
import {OneTimeTestService} from '../../../services/one-time-test.service';

@Component({
  selector: 'app-test-description',
  templateUrl: './test-description.component.html',
  styleUrls: ['./test-description.component.scss']
})
export class TestDescriptionComponent implements OnInit, OnDestroy, DoCheck {
  public test: Test;
  private subscription: Subscription;
  public oneTimeTest: OneTimeTest;
  private oneTimeTestSubscription: Subscription;

  constructor(private testingService: TestContainerService,
              private alertService: AlertService,
              private router: Router,
              private oneTimeTestService: OneTimeTestService,
              private activatedRoute: ActivatedRoute,
              private oneTimeTestingService: TestContainerOneTimeService) {}

  ngOnInit() {
    this.oneTimeTestSubscription = this.oneTimeTestingService.oneTimeTestLoaded.subscribe(
      (oneTimeTest: OneTimeTest) => {
        this.oneTimeTest = oneTimeTest;
        if (this.oneTimeTest && this.oneTimeTestFinishedOrExpired() && this.oneTimeTest.result) {
          this.alertService.success('oneTimeTestFinished', true);
          this.router.navigate(['/result', this.oneTimeTest.result]);
        }
      }
    );
    this.subscription = this.testingService.testLoaded.subscribe(
      (test: Test) => {
        this.test = test;
      }
    );
    this.testingService.getTest();
  }

  // clicking back button in browser leads to description page
  // without this check oneTimeTest status seems to be not started
  ngDoCheck() {
    if (!this.oneTimeTest) {
      this.oneTimeTestingService.getOneTimeTest();
    }
  }

  oneTimeTestFinishedOrExpired() {
    return this.oneTimeTest && (this.oneTimeTest.status === 'finished' || this.oneTimeTest.status === 'expired');
  }

  oneTimeTestAlreadyStarted() {
    return this.oneTimeTest && this.oneTimeTest.status === 'started';
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.oneTimeTestSubscription) {
      this.oneTimeTestSubscription.unsubscribe();
    }
  }

  startTest() {
    if (!this.oneTimeTest) {
      this.testingService.startTest();
      this.router.navigate(['../'], {relativeTo: this.activatedRoute});
      return;
    }
    if (!this.oneTimeTest.assignee || this.oneTimeTest.assignee === '') {
      this.alertService.error('enterName');
      return;
    }
    this.oneTimeTestService.setAssignee(this.oneTimeTest)
      .subscribe(
        (oneTimeTest: OneTimeTest) => {
          this.oneTimeTest = oneTimeTest;
          this.testingService.startTest();
          this.router.navigate(['../'], {relativeTo: this.activatedRoute});
        },
        error => {
          this.alertService.error(error._body);
        }
      );
  }

  joinTags() {
    const topics = [];
    const tags = this.test.tags;
    if (tags && tags.length > 0) {
      for (let i = 0; i < tags.length; i++) {
        topics.push(tags[i].title);
      }
      return topics.join(', ');
    }
    return '';
  }

}
