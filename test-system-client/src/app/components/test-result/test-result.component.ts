import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Test} from "../../models/test.model";
import {Result} from "../../models/result.model";
import {ResultService} from "../../services/result.service";
import {AlertService} from '../../services/alert.service';


@Component({
  selector: 'app-test-result',
  templateUrl: './test-result.component.html',
  styleUrls: ['./test-result.component.scss']
})
export class TestResultComponent implements OnInit {
  resultUrl = '';
  isCopied = false;
  result: Result;
  test: Test;
  timeUsed = 0;
  scores: string;
  currentUser;
  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private alertService: AlertService,
              private resultService: ResultService) {
  }

  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.resultUrl = window.location.href;
    const resultId = this.activatedRoute.snapshot.params['id'];

    this.resultService.getById(resultId)
      .subscribe(
        (result) => {
          this.result = result;
          this.scores = (100 * result.correctAnswers / result.questionsNumber).toFixed(2);
          this.timeUsed = this.result.endTime - this.result.startTime;
          this.test = result.test;
        },
        (error) => {
          this.alertService.error(error._body, true);
          this.router.navigate(['/']);
        }
      );
  }

  viewerIsOwner() {
    return (this.currentUser && this.result.user && this.currentUser._id === this.result.user._id);
  }

  toggleShareResult() {
    this.resultService.toggleShareResult(this.result)
      .subscribe(
        (result) => {
          this.result = result;
        }
      );
  }
}
