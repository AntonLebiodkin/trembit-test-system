import { Component, OnInit } from '@angular/core';
import { User } from "../../models/user.model";
import { UserService } from "../../services/user.service";
import { AlertService } from "../../services/alert.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  user: User;

  constructor(private userService: UserService, private alertService: AlertService) {}

  ngOnInit() {
    this.userService.getCurrent()
      .subscribe(
        data => {
          this.user = data;
        },
        error => {
          this.alertService.error(error._body);
        }
      )
  }
}
