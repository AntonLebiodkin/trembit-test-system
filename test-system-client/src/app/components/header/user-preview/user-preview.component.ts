import { Component, DoCheck, ElementRef, HostListener } from '@angular/core';
import { User } from "../../../models/user.model";
import {AuthenticationService} from "../../../services/authentication.service";
import {Router} from "@angular/router";
import {AuthService} from "angular2-social-login";

@Component({
  selector: 'app-user-preview',
  templateUrl: './user-preview.component.html',
  styleUrls: ['./user-preview.component.scss'],
})
export class UserPreviewComponent implements DoCheck {
  user: User;
  dropdownClass = 'hidden-class';

  constructor(private authenticationService: AuthenticationService,
              private router: Router,
              private elRef: ElementRef,
              public _auth: AuthService) {
  }

  toggleDropdown() {
    this.dropdownClass = this.dropdownClass === 'hidden-class' ? 'shown-class' : 'hidden-class';
  }

  ngDoCheck() {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  userIsAdmin() {
    return this.authenticationService.isAdmin();
  }

  @HostListener('document:click') onClick() {
    if (this.dropdownClass !== 'hidden-class') {
      let parent = event.srcElement;
      while (parent = parent.parentElement) {
        if (parent === this.elRef.nativeElement) {
          break;
        }
      }
      if (!parent) {
        this.dropdownClass = 'hidden-class';
      }
    }
  };

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['']);
    this._auth.logout().subscribe(
      (data) => {console.log('data', data); }
    );
  }
}
