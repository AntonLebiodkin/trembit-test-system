import {AfterViewChecked, Component, Input, OnChanges, OnDestroy, OnInit, ViewContainerRef} from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { MODES } from './popup.config';
import {DomSanitizer} from '@angular/platform-browser';
import {HighlightJsService} from 'angular2-highlight-js';
import {TranslateService} from 'ng2-translate';

let currentMode;

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss'],
  providers: [Modal]
})
export class PopupComponent implements OnInit, OnDestroy, OnChanges, AfterViewChecked {
  @Input() mode;
  @Input() show: boolean;
  @Input() onCloseFunction: Function;
  @Input() onConfirmFunction: Function;
  @Input() bodyData: any;
  currentlyVisible = false;
  popupDestroySubscription: Subscription;
  shouldHighlight = false;

  constructor(public modal: Modal,
              private sanitizer: DomSanitizer,
              private highlightService: HighlightJsService,
              private translateService: TranslateService,
              vcRef: ViewContainerRef) {
    modal.overlay.defaultViewContainer = vcRef;
  }

  ngOnInit() {

  }

  ngOnChanges(changes: any) {
    if (this.show && !this.currentlyVisible) {
      if (this.popupDestroySubscription) {
        this.popupDestroySubscription.unsubscribe();
      }
      this.currentlyVisible = true;
      currentMode = MODES[this.mode];
      if (this.bodyData) {
        if (this.mode === 'previewQuestion') {
          // get rid of ugly spaces before sanitizing
          this.bodyData = this.bodyData.replace(/&nbsp;/g, ' ');
          currentMode.body = this.sanitizer.bypassSecurityTrustHtml(this.bodyData);
        }
      }
      this.showPopup();
    }
  }

  ngOnDestroy() {
    if (this.popupDestroySubscription) {
      this.popupDestroySubscription.unsubscribe();
    }
  }

  showPopup() {
    let body = currentMode.body;
    if (currentMode.body.length) {
      this.translateService.get(currentMode.body).subscribe((translated) => {
        body = translated;
      });
    }
    let okButtonText = currentMode.okBtn;
    if (currentMode.okBtn.length) {
      this.translateService.get(currentMode.okBtn).subscribe((translated) => {
        okButtonText = translated;
      });
    }
    if (currentMode.type === 'alert') {
      this.modal.alert()
        .size(currentMode.size)
        .isBlocking(currentMode.isBlocking)
        .showClose(currentMode.showClose)
        .keyboard(currentMode.keyboard)
        .dialogClass(currentMode.dialogClass)
        .headerClass(currentMode.headerClass)
        .title(currentMode.title)
        .titleHtml(currentMode.titleHtml)
        .body(body)
        .bodyClass(currentMode.bodyClass)
        .footerClass(currentMode.footerClass)
        .okBtn(okButtonText)
        .okBtnClass(currentMode.okBtnClass)
        .open()
        .then(
          (modal => {
            this.shouldHighlight = true;
            this.popupDestroySubscription = modal.onDestroy.subscribe(
              () => {
                this.currentlyVisible = false;
                if (this.onCloseFunction) {
                  this.onCloseFunction();
                }
              }
            );
          })
        );
    } else if (currentMode.type === 'confirm') {
      this.modal.confirm()
        .size(currentMode.size)
        .isBlocking(currentMode.isBlocking)
        .showClose(currentMode.showClose)
        .keyboard(currentMode.keyboard)
        .dialogClass(currentMode.dialogClass)
        .headerClass(currentMode.headerClass)
        .title(currentMode.title)
        .titleHtml(currentMode.titleHtml)
        .body(body)
        .bodyClass(currentMode.bodyClass)
        .footerClass(currentMode.footerClass)
        .okBtn(okButtonText)
        .okBtnClass(currentMode.okBtnClass)
        .open()
        .then(
          (modal => {
            this.shouldHighlight = true;
            modal.result.then(
              (confirmed) => {
                if (confirmed && this.onConfirmFunction) {
                  this.onConfirmFunction();
                }
              }
            );
            this.popupDestroySubscription = modal.onDestroy.subscribe(
              () => {
                this.currentlyVisible = false;
                if (this.onCloseFunction) {
                  this.onCloseFunction();
                }
              }
            );
          })
        );
    }
  }

  ngAfterViewChecked() {
    if (this.shouldHighlight) {
      this.shouldHighlight = false;
      const modalOverlay =  document.querySelector('modal-overlay');
      const codeBlocks = modalOverlay.querySelectorAll('code');
      for (let i = 0; i < codeBlocks.length; i++) {
        codeBlocks[i].innerText = codeBlocks[i].innerHTML;
        this.highlightService.highlight(codeBlocks[i]);
      }
    }
  }
}
