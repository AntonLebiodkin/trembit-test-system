export const MODES = {
  testAutoFinishPopup: {
    size: 'lg',
    isBlocking: true,
    showClose: false,
    dialogClass: 'modal-dialog',
    headerClass: 'modal-header',
    title: '',
    titleHtml: '',
    body: 'popup.timeRanOut',
    bodyClass: 'modal-body',
    footerClass: 'modal-footer',
    okBtn: 'popup.finishTest',
    okBtnClass: 'btn btn-primary',
    type: 'alert'
  },
  testFinishWarning: {
    size: 'lg',
    isBlocking: false,
    showClose: false,
    dialogClass: 'modal-dialog',
    headerClass: 'modal-header',
    title: '',
    titleHtml: '',
    body: 'popup.finishTestConfirm',
    bodyClass: 'modal-body',
    footerClass: 'modal-footer',
    okBtn: 'popup.toResult',
    okBtnClass: 'btn btn-success',
    type: 'confirm'
  },
  testDeactivateWarning: {
    size: 'lg',
    isBlocking: false,
    showClose: false,
    dialogClass: 'modal-dialog',
    headerClass: 'modal-header',
    title: '',
    titleHtml: '',
    body: 'popup.leaveWarn',
    bodyClass: 'modal-body',
    footerClass: 'modal-footer',
    okBtn: 'popup.leave',
    okBtnClass: 'btn btn-danger',
    type: 'confirm'
  },
  previewQuestion: {
    size: 'lg',
    isBlocking: false,
    showClose: false,
    dialogClass: 'modal-dialog',
    headerClass: 'modal-header',
    title: '',
    titleHtml: '',
    body: '',
    bodyClass: 'modal-body-left',
    footerClass: 'modal-footer',
    okBtn: 'popup.close',
    okBtnClass: 'btn btn-success',
    type: 'alert'
  }
}

