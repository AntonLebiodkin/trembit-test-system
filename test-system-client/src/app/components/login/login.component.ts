import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from "../../services/authentication.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AlertService } from "../../services/alert.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: any = {};
  loading = false;
  returnUrl: string;



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) {  }


  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.user.email, this.user.password)
      .subscribe(
        (data) => {
          this.router.navigate([this.returnUrl]);
        },
        (error) => {
          this.alertService.error(error._body);
          this.loading = false;
        }
      );
  }

  loginWithOptions(prov: string) {
    this.loading = true;
    this.authenticationService.loginWithOptions(prov)
      .subscribe(
        (data) => {
          data.subscribe(
            (res) => {
              this.router.navigate([this.returnUrl]);
            },
            (error) => {
              this.alertService.error(error._body);
              this.loading = false;
            }
          );
        },
        (error) => {
          this.alertService.error(error._body);
        }
      );
  }
}
