import {Component, OnInit} from '@angular/core';
import {ResultService} from "../../../services/result.service";
import {Result} from "../../../models/result.model";
import {TestService} from "../../../services/test.service";

@Component({
  selector: 'app-view-results',
  templateUrl: './view-results.component.html',
  styleUrls: ['./view-results.component.scss']
})
export class ViewResultsComponent implements OnInit {
  page: number;
  maxSize = 4;

  results: Result[];

  constructor(private resultService: ResultService, private testService: TestService) { }

  ngOnInit() {
    this.resultService.getAll()
      .subscribe(
        results => {
          this.results = results;
        }
      );
  }
}
