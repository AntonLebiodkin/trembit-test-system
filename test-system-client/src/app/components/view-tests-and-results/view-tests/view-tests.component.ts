import {Component, OnDestroy, OnInit} from '@angular/core';
import { TestService } from '../../../services/test.service';
import { Test } from '../../../models/test.model';
import { AlertService } from '../../../services/alert.service';
import {FilteringByTagsService} from '../../../services/filtering-by-tags.service';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-view-tests',
  templateUrl: './view-tests.component.html',
  styleUrls: ['./view-tests.component.scss']
})
export class ViewTestsComponent implements OnInit, OnDestroy {
  // pagination
  page = 1;
  maxSize = 4;
  // pagination end

  // filter vars block
  filtersChangedSubscription: Subscription;
  filteredTests = [];
  // filter vars block end

  tests: Test[];

  constructor(private testService: TestService,
              private filteringByTagsService: FilteringByTagsService,
              private alertService: AlertService) { }

  ngOnInit() {
    this.testService.getAvailable()
      .subscribe(
        (tests) => {
          this.tests = tests;
          this.filteringByTagsService.initTagFilters(this.tests, this.filteredTests);
        },
        (error) => {
          this.alertService.error(error._body);
        }
      );
    this.filtersChangedSubscription = this.filteringByTagsService.filtersChanged.subscribe(
      (filters) => {
        this.filteredTests = this.filteringByTagsService.getFilteredArray(this.tests, this.filteredTests);
      }
    );
  };

  ngOnDestroy() {
    if (this.filtersChangedSubscription) {
      this.filtersChangedSubscription.unsubscribe();
    }
  }
}
