import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { AlertService } from "../../services/alert.service";
import { Router } from "@angular/router";
import { UserService } from "../../services/user.service";
import { User } from "../../models/user.model";
import { AuthenticationService } from "../../services/authentication.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  user: any = {};
  registerForm: FormGroup;
  submitted = false;
  loading = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private authenticationService: AuthenticationService,
              private alertService: AlertService,
              private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(/^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i)
        ],
        [this.userService.emailExists.bind(this.userService)]
      ],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: [],
    },
    {
      validator: this.matchingPasswords('password', 'confirmPassword')
    });
  }

  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): {[key: string]: any} => {
      const password = group.controls[passwordKey];
      const confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }

  register() {
    this.submitted = true;
    if (!this.registerForm.errors) {
      this.loading = true;
      this.userService.create(this.registerForm.value as User)
        .subscribe(
          (data) => {
            this.alertService.success('registered', true);
            return this.authenticationService.login(this.registerForm.value.email, this.registerForm.value.password).subscribe(
              (res) => {
                this.router.navigate(['/profile']);
              }
            );
          },
          (error) => {
            this.alertService.error(error._body);
            this.loading = false;
          });
    } else {
      this.alertService.error('invalidForm');
    }
  }


  registerWithOptions(prov: string) {
      this.loading = true;
      this.authenticationService.loginWithOptions(prov)
        .subscribe(
          (data) => {
            data.subscribe(
              (res) => {
                this.router.navigate(['/profile']);
              },
              (error) => {
                this.alertService.error(error._body);
                this.loading = false;
              }
            );
          },
          (error) => {
            this.alertService.error(error._body);
          }
        );
    }
}
