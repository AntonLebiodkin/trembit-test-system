import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {Http, HttpModule} from '@angular/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ProfileComponent } from './components/profile/profile.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AlertComponent } from './components/alert/alert.component';
import { TestResultComponent } from './components/test-result/test-result.component';
import { AdminModule } from './components/admin/admin.module';
import { AlertService } from './services/alert.service';
import { ResultService } from './services/result.service';
import { AuthenticationService } from './services/authentication.service';
import { UserService } from './services/user.service';
import { TestService } from './services/test.service';
import { QuestionService } from './services/question.service';
import { AdminGuard } from './guards/admin.guard';
import { AuthGuard } from './guards/auth.guard';
import { ViewTestsComponent } from './components/view-tests-and-results/view-tests/view-tests.component';
import { Angular2SocialLoginModule } from 'angular2-social-login';
import { UserPreviewComponent } from './components/header/user-preview/user-preview.component';
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { ViewResultsComponent } from './components/view-tests-and-results/view-results/view-results.component';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import {TestContainerModule } from './components/test-container/test-container.module';
import { ViewTestsAndResultsComponent } from './components/view-tests-and-results/view-tests-and-results.component';
import { SharedModule } from './shared/shared.module';
import { ProfileEditComponent } from './components/profile-edit/profile-edit.component';
import { HighlightJsService } from 'angular2-highlight-js';
import { OneTimeTestService} from './services/one-time-test.service';
import { TagService } from './services/tag.service';
import { FilteringByTagsService } from './services/filtering-by-tags.service';
import {Ng2DragDropService} from 'ng2-drag-drop/src/services/ng2-drag-drop.service';
import {TranslateLoader, TranslateModule, TranslateStaticLoader} from 'ng2-translate';


const providers = {
    'google': {
      'clientId': '487317087913-4hj8apbjn6qfua68lvaqg8ivsoigmn9h.apps.googleusercontent.com'
    },
    'facebook': {
      'clientId': '222338791607609',
      'apiVersion': 'v2.9' // like v2.4
    }
  };

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    HeaderComponent,
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    HomeComponent,
    PageNotFoundComponent,
    AlertComponent,
    ProfileComponent,
    ViewTestsComponent,
    ViewResultsComponent,
    UserPreviewComponent,
    TestResultComponent,
    ViewResultsComponent,
    ViewTestsAndResultsComponent,
    ProfileEditComponent
  ],
  imports: [
    AdminModule,
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    Angular2SocialLoginModule,
    ModalModule.forRoot(),
    NgxPaginationModule,
    BootstrapModalModule,
    Ng2DragDropModule,
    NgxPaginationModule,
    TestContainerModule,
    SharedModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    })
  ],
  providers: [
    AuthGuard,
    AdminGuard,
    UserService,
    TestService,
    AlertService,
    AuthenticationService,
    QuestionService,
    ResultService,
    HighlightJsService,
    OneTimeTestService,
    TagService,
    FilteringByTagsService,
    Ng2DragDropService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {}
}

Angular2SocialLoginModule.loadProvidersScripts(providers);
