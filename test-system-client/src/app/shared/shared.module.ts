import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PopupComponent} from "../components/popup/popup.component";
import {ClipboardModule} from "ngx-clipboard/dist";
import { FilterBoxComponent } from '../components/filter-box/filter-box.component';
import {SortingService} from '../components/admin/sorting.service';
import {TranslateModule} from 'ng2-translate';

@NgModule({
  exports: [
    CommonModule,
    PopupComponent,
    ClipboardModule,
    FilterBoxComponent,
    TranslateModule
  ],
  declarations: [
    PopupComponent,
    FilterBoxComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [
    SortingService
  ]
})
export class SharedModule { }
