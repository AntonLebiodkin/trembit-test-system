import {PreloadAllModules, RouterModule, Routes} from "@angular/router";
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProfileComponent } from "./components/profile/profile.component";
import { AuthGuard } from "./guards/auth.guard";
import { TestResultComponent } from './components/test-result/test-result.component';

import { NgModule } from "@angular/core";
import {ViewTestsAndResultsComponent} from "./components/view-tests-and-results/view-tests-and-results.component";
import {ProfileEditComponent} from "./components/profile-edit/profile-edit.component";

export const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'profile/edit', component: ProfileEditComponent, canActivate: [AuthGuard]},
  { path: 'test/:id',
    loadChildren: './components/test-container/test-container.module#TestContainerModule'
  },
  { path: 'one-time-test/:oneTimeId',
    loadChildren: './components/test-container/test-container.module#TestContainerModule'
  },
  { path: 'result/:id', component: TestResultComponent},
  { path: 'tests', component: ViewTestsAndResultsComponent, canActivate: [AuthGuard]},
  { path: 'admin',
    loadChildren: './components/admin/admin.module#AdminModule'
  },
  { path: '', component: HomeComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
