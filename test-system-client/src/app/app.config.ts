import { environment } from '../environments/environment';

export class AppConfig {
  public static apiUrl = environment.production ? 'http://192.168.1.101:3000' : 'http://localhost:3000';
  public static frontUrl = environment.production ? 'http://trembit.com/sandbox/testing-system' : 'http://localhost:4200';
};
