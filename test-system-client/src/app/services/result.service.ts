import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import {AppConfig} from "../app.config";
import {AuthenticationService} from "./authentication.service";
import {Result} from '../models/result.model';

@Injectable()
export class ResultService {
  private jwt = AuthenticationService.jwt;

  constructor(private http: Http) { }

  getAll() {
    return this.http.get(AppConfig.apiUrl + '/results', this.jwt()).map((response: Response) => response.json());
  }

  getById(_id: string) {
    return this.http.get(AppConfig.apiUrl + '/results/' + _id, this.jwt()).map((response: Response) => response.json());
  }

  toggleShareResult(result: Result) {
    return this.http.post(AppConfig.apiUrl + '/results/' + result._id + '/toggleShare', result, this.jwt())
      .map((response: Response) => response.json());
  }
}
