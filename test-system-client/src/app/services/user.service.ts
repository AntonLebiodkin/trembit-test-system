import { Injectable } from '@angular/core';
import {Http, Response, URLSearchParams} from '@angular/http';
import { User } from '../models/user.model';
import { Observable } from 'rxjs';
import { Result } from '../models/result.model';
import { AppConfig } from '../app.config';
import {AuthenticationService} from './authentication.service';

@Injectable()
export class UserService {
  private jwt = AuthenticationService.jwt;

  constructor(private http: Http) { }

  emailExists(control: any): Observable<any> {
    const params: URLSearchParams = new URLSearchParams();
    params.set('email', control.value);

    return this.http.get(AppConfig.apiUrl + '/users/emailExists', { search: params}).map((response: Response) => {
      const res = response.json();
      if (res.exists) {
        return {'emailExists': true};
      }
    });
  }

  toggleAdmin(id: number) {
    return this.http.post(AppConfig.apiUrl + '/users/updatePermissions', { userId: id, permission: 'admin' }, this.jwt())
                    .map((response: Response) => response.json());
  }

  getAll(): Observable<User[]> {
    return this.http.get(AppConfig.apiUrl + '/users', this.jwt()).map((response: Response) => response.json());
  }

  getCurrent(): Observable<User> {
    return this.http.get(AppConfig.apiUrl + '/users/current', this.jwt())
      .map((response: Response) => response.json());
  }

  getUserById(_id: string) {
    return this.http.get(AppConfig.apiUrl + '/users/' + _id, this.jwt())
      .map((response: Response) => response.json());
  }

  create(user: User) {
    return this.http.post(AppConfig.apiUrl + '/users/register', user, this.jwt());
  }

  update(user: User, emailCredentials) {
    return this.http.put(AppConfig.apiUrl + '/users/' + user._id,
      {user: user, emailCredentials: emailCredentials}, this.jwt())
      .map((response: Response) => response.json());
  }

  assignTest(userId: string, testId: string) {
    return this.http.post(AppConfig.apiUrl + '/users/assignTest/', { userId: userId, testId: testId }, this.jwt())
                    .map((response: Response) => response.json());
  }

  unassignTest(userId: string, testId: string) {
    return this.http.post(AppConfig.apiUrl + '/users/unassignTest/', { userId: userId, testId: testId }, this.jwt())
      .map((response: Response) => response.json());
  }

  completeTest(result: Result, makeResultShared = false) {
    return this.http.post(AppConfig.apiUrl + '/users/complete', {result: result, shared: makeResultShared});
  }

  _delete(_id: string) {
    return this.http.delete(AppConfig.apiUrl + '/users/' + _id, this.jwt());
  }
}
