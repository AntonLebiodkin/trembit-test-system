import { Injectable } from '@angular/core';
import {AuthenticationService} from './authentication.service';
import { Http, Response } from '@angular/http';
import {AppConfig} from '../app.config';
import {Tag} from '../models/tag.model';

@Injectable()
export class TagService {

  private jwt = AuthenticationService.jwt;

  constructor(private http: Http) { }

  getAll() {
    return this.http.get(AppConfig.apiUrl + '/tags', this.jwt())
      .map((response: Response) => response.json());
  }

  update(tag: Tag) {
    return this.http.put(AppConfig.apiUrl + '/tags/' + tag._id, tag, this.jwt())
      .map((response: Response) => response.json());
  }

  create(tag: Tag) {
    return this.http.post(AppConfig.apiUrl + '/tags/', tag, this.jwt())
      .map((response: Response) => response.json());
  }

  _delete(_id: string) {
    return this.http.delete(AppConfig.apiUrl + '/tags/' + _id, this.jwt());
  }

}
