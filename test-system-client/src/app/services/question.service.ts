import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {AppConfig} from "../app.config";
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class QuestionService {
  private jwt = AuthenticationService.jwt;

  constructor(private http: Http) { }

  getAll() {
    return this.http.get(AppConfig.apiUrl + '/questions', this.jwt()).map(
      (response: Response) => response.json()
    );
  }

  getById(_id: string) {
    return this.http.get(AppConfig.apiUrl + '/questions/' + _id, this.jwt()).map((response: Response) => response.json());
  }

  create(question: any) {
    return this.http.post(AppConfig.apiUrl + '/questions/create', question, this.jwt());
  }

  delete(_id: string) {
    return this.http.delete(AppConfig.apiUrl + '/questions/' + _id, this.jwt());
  }

  update(question: any) {
    return this.http.put(AppConfig.apiUrl + '/questions/' + question._id, question, this.jwt());
  }
}
