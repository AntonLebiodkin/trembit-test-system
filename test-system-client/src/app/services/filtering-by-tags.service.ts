import {Injectable} from '@angular/core';
import {Filter} from '../models/filter.model';
import {Subject} from 'rxjs/Subject';
import {Tag} from '../models/tag.model';

@Injectable()
export class FilteringByTagsService {
  filtersChanged = new Subject<Filter[]>();
  activeFilters: number;
  activeFiltersIncreased: boolean;

  private filters: Filter[];
  constructor() { }

  initTagFilters(data: {tags: Tag[]}[], filteredData: {tags: Tag[]}[]) {
    this.filters = [];
    this.activeFiltersIncreased = false;
    this.activeFilters = 0;
    for (let i = 0; i < data.length; i++) {
      filteredData.push(data[i]);
    }
    const uniqueTags = this.getUniqueTags(data);
    this.setFilters(uniqueTags);
    this.getFilters();
  }

  private getUniqueTags(data: {tags: Tag[]}[]): Tag[] {
    // get array of unique tags for filtering
    const uniqueTags = [];
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < data[i].tags.length; j++) {
        let unique = true;
        for (let k = 0; k < uniqueTags.length; k++) {
          if (uniqueTags[k]._id === data[i].tags[j]._id) {
            unique = false;
            break;
          }
        }
        if (unique) {
          uniqueTags.push(data[i].tags[j]);
        }
      }
    }
    return uniqueTags;
  }

  private setFilters(tags: Tag[], filters = null) {
    this.activeFilters = 0;
    tags.forEach(
      (tag) => {
        let newFilter: Filter;
        if (!filters) {
          newFilter = new Filter(tag.title);
        } else {
          let active = false;
          const filterIndex = filters.findIndex(
            (filter) => {
              return filter.name === tag.title;
            }
          );
          if (filterIndex !== -1) {
            active = filters[filterIndex].active;
            if (active) {
              this.activeFilters++;
            }
          }
          newFilter = new Filter(tag.title, active);
        }
        this.filters.push(newFilter);
      }
    );
  }

  // if one of objects with tags was deleted
  recalculateFilters(data: {tags: Tag[]}[]) {
    const backupFilters = this.filters.slice();
    this.filters = [];
    const uniqueTags = this.getUniqueTags(data);
    this.setFilters(uniqueTags, backupFilters);
    this.getFilters();
  }

  // emit to all subscribers to recalculate filtered arrays
  getFilters() {
    this.filtersChanged.next(this.filters.slice());
  }

  changeFilter(index: number) {
    this.filters[index].active = !this.filters[index].active;
    if (this.filters[index].active) {
      this.activeFilters++;
      this.activeFiltersIncreased = true;
    } else {
      this.activeFilters--;
      this.activeFiltersIncreased = false;
    }
    this.getFilters();
  }

  // without count it goes infinite loop
  // => getFilteredArray => activeFilters === 0 => cleanFilters => emit filters changed =>
  clearFilters() {
    let count = 0;
    for (let i = 0; i < this.filters.length; i++) {
      if (this.filters[i].active) {
        this.filters[i].active = false;
        count++;
      }
    }
    if (count) {
      this.activeFilters = 0;
      this.getFilters();
    }
  }

  getFilteredArray(data: {tags: Tag[]}[], filteredData: {tags: Tag[]}[]): {tags: Tag[]}[] {
    if (this.activeFilters === 0) {
      this.clearFilters();
      return data ? data.slice() : [];
    }
    // refilter already filtered values
    if (this.activeFiltersIncreased) {
      return this.filterArray(filteredData);
    }
    return this.filterArray(data);
  }

  private hasAllTags(data: {tags: Tag[]}) {
    if (data.tags.length < this.activeFilters) {
      return false;
    }
    let filteredTags = 0;
    for (let i = 0; i < data.tags.length; i++) {
      for (let j = 0; j < this.filters.length; j++) {
        if (this.filters[j].active && this.filters[j].name === data.tags[i].title) {
          filteredTags++;
          break;
        }
      }
      if (filteredTags === this.activeFilters) {
        return true;
      }
    }
  }

  private filterArray(data: {tags: Tag[]}[]) {
    return data.filter(this.hasAllTags.bind(this));
  }

  anyActiveFilters() {
    return this.activeFilters !== 0;
  }
}
