import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Test } from "../models/test.model";
import {AppConfig} from "../app.config";
import {AuthenticationService} from "./authentication.service";

@Injectable()
export class TestService {
  private jwt = AuthenticationService.jwt;

  constructor(private http: Http) { }

  getAll() {
    return this.http.get(AppConfig.apiUrl + '/tests', this.jwt()).map((response: Response) => response.json());
  }

  getAvailable() {
    return this.http.get(AppConfig.apiUrl + '/tests/available', this.jwt()).map((response: Response) => response.json());
  }

  getPrivateUserTests() {
    return this.http.get(AppConfig.apiUrl + '/tests/assigned', this.jwt()).map((response: Response) => response.json());
  }

  getById(_id: string) {
    return this.http.get(AppConfig.apiUrl + '/tests/' + _id, this.jwt()).map((response: Response) => response.json());
  }

  update(test: Test) {
    return this.http.put(AppConfig.apiUrl + '/tests/' + test._id, test, this.jwt());
  }

  create(test: Test) {
    return this.http.post(AppConfig.apiUrl + '/tests/create', test, this.jwt());
  }

  delete(_id: string) {
    return this.http.delete(AppConfig.apiUrl + '/tests/' + _id, this.jwt());
  }

}
