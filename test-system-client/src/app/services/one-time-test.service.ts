import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import {AppConfig} from "../app.config";
import {OneTimeTest} from '../models/one-time-test.model';
import {AuthenticationService} from './authentication.service';

@Injectable()
export class OneTimeTestService {
  private jwt = AuthenticationService.jwt;
  constructor(private http: Http) { }

  getAll() {
    return this.http.get(AppConfig.apiUrl + '/oneTimeTests', this.jwt()).map((response: Response) => response.json());
  }

  getById(_id: string) {
    return this.http.get(AppConfig.apiUrl + '/oneTimeTests/' + _id).map((response: Response) => response.json());
  }

  update(oneTimeTest: OneTimeTest) {
    return this.http.put(AppConfig.apiUrl + '/oneTimeTests/' + oneTimeTest._id, oneTimeTest, this.jwt())
      .map((response: Response) => response.json());
  }

  updateStatus(oneTimeTest: OneTimeTest) {
    return this.http.put(AppConfig.apiUrl + '/oneTimeTests/' + oneTimeTest._id + '/setStatus', {status: oneTimeTest.status}, null)
      .map((response: Response) => response.json());
  }

  setAssignee(oneTimeTest: OneTimeTest) {
    return this.http.post(AppConfig.apiUrl + '/oneTimeTests/' + oneTimeTest._id + '/setAssignee', {assignee: oneTimeTest.assignee}, null)
      .map((response: Response) => response.json());
  }

  setResult(oneTimeTest: OneTimeTest) {
    return this.http.put(AppConfig.apiUrl + '/oneTimeTests/' + oneTimeTest._id + '/setResult', oneTimeTest.result, null)
      .map((response: Response) => response.json());
  }

  create(oneTimeTest: OneTimeTest) {
    return this.http.post(AppConfig.apiUrl + '/oneTimeTests/create', oneTimeTest, this.jwt())
      .map((response: Response) => response.json());
  }

  delete(_id: string) {
    return this.http.delete(AppConfig.apiUrl + '/oneTimeTests/' + _id, this.jwt());
  }
}
