import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import {TranslateService} from 'ng2-translate';

@Injectable()
export class AlertService {
  private subject = new Subject<any>();
  private keepAfterNavigationChange = false;

  constructor(private router: Router,
              private translateService: TranslateService) {
    // clear alert message on route change
    router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if (this.keepAfterNavigationChange) {
          // only keep for a single location change
          this.keepAfterNavigationChange = false;
        } else {
          // clear alert
          this.subject.next();
        }
      }
    });
  }

  success(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    let translatedMessage = message;
    if (translatedMessage.length) {
      this.translateService.get('alert.success.' + message).subscribe((translation) => {
          translatedMessage = translation;
      });
    }
    this.subject.next({ type: 'success', text: translatedMessage });
  }

  error(message: string, keepAfterNavigationChange = false) {
    this.keepAfterNavigationChange = keepAfterNavigationChange;
    let translatedMessage = 'alert.error.' + message;
    if (translatedMessage.length) {
      this.translateService.get(translatedMessage).subscribe((translation) => {
        if (translation) {
          translatedMessage = translation;
        }
      });
    }
    this.subject.next({ type: 'error', text: translatedMessage });
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
