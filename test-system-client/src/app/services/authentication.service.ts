import { Injectable } from '@angular/core';
import {Http, RequestOptions, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {AppConfig} from '../app.config';
import { AuthService } from 'angular2-social-login';
import {TranslateService} from 'ng2-translate';


@Injectable()
export class AuthenticationService {
  config: any = '../app.config';

  public static jwt() {
    // create authorization header with jwt token
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = JSON.parse(localStorage.getItem('token'));
    if (currentUser && token) {
      const headers = new Headers({ 'Authorization': 'Bearer ' + token });
      return new RequestOptions({ headers: headers });
    }
  }

  constructor(public _auth: AuthService,
              private translate: TranslateService,
              private http: Http) {}

  login(email: string, password: string) {
    return this.http.post(AppConfig.apiUrl + '/users/authenticate/', { email: email, password: password })
      .map((response: Response) => {
        const userWithToken = response.json();
        this.setLocalUser(userWithToken);
    });
  }

  loginWithOptions(prov: string) {
    return this._auth.login(prov)
      .map(
        (data) => {
          return this.http.post(AppConfig.apiUrl + '/users/authenticate/social', data)
            .map((response: Response) => {
              const userWithToken = response.json();
              this.setLocalUser(userWithToken);
            });
      });
  }

  loggedIn() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    const token = JSON.parse(localStorage.getItem('token'));
    return user !== null && token !== null;
  }

  isAdmin() {
    const user = JSON.parse(localStorage.getItem('currentUser'));
    return user !== null && user.permissions && user.permissions.includes('admin');
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this.translate.use('en');
    this._auth.logout().subscribe(
      (data) => {
        return data;
      }
    );
  }

  private setLocalUser(userWithToken: any) {
    const user = userWithToken.user;
    const token = userWithToken.token;
    if (user && token) {
      localStorage.setItem('currentUser', JSON.stringify(user));
      localStorage.setItem('token', JSON.stringify(token));
    }
    this.translate.use(user.language);
  }

}
