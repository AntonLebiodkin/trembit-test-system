import {Tag} from './tag.model';

export class Question {
  _id: string;
  question: string;
  options: string[];
  type: string;
  status: string;
  tags: Tag[];

  constructor() {
    this.status = 'clear';
  }
}
