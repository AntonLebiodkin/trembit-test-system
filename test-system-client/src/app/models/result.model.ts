import {User} from "./user.model";
import {Test} from "./test.model";
export class Result {
  _id: string;
  user: User;
  test: Test;
  testTopic: string;
  userAnswers: any[];
  correctAnswers: number;
  questionsNumber: number;
  defaultTime: number;
  startTime: number;
  endTime: number;
  shared: boolean;
  constructor(user: User, test: Test, userAnswers: any[]) {
    this.user = user;
    this.test = test;
    this.defaultTime = test.time;
    this.testTopic = test.topic;
    this.questionsNumber = test.questions.length;
    this.userAnswers = userAnswers;
    this.correctAnswers = 0;
  }
}
