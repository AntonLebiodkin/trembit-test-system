import {Test} from './test.model';
import {Result} from './result.model';
export class OneTimeTest {
  _id?: string;
  assignee?: string;
  test: Test;
  result: Result;
  status: string;
  expireDate: number;

  constructor() {
    this.status = 'created';
    this.expireDate = Date.now() + 24 * 3600 * 1000;
  }
}
