import { Tag } from './tag.model';
export class Test {
  _id?: string;
  topic: string;
  description: string;
  tags: Tag[];
  questions: any[];
  accessType?: string;
  time: number;
}
