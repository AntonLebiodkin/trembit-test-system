export class User {
  _id: string;
  permissions: string[];
  firstName: string;
  lastName: string;
  birthDate: Date;
  imageUrl: string;
  socials: {
    provider: string;
    email: string;
  }[];
  emailCredentials: {
    email: string;
  };
  language: string;
}
