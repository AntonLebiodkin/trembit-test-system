import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from "rxjs/Observable";
import {UserService} from "../services/user.service";
import {AlertService} from "../services/alert.service";

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private router: Router,
              private alertService: AlertService,
              private userService: UserService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|boolean {
    return this.userService.getCurrent().map(
      (user: any) => {
        if (user && user.permissions && user.permissions.indexOf('admin') !== -1) {
          return true;
        } else {
          this.alertService.error('notAdmin', true);
          this.router.navigate(['/profile']);
          return false;
        }
      },
      (error) => {
        this.alertService.error(error._body, true);
        this.router.navigate(['/profile']);
        return false;
      }
    );
  }
}
