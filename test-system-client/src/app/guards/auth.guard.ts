import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {AlertService} from "../services/alert.service";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private alertService: AlertService) { }

  canActivate() {
    if (localStorage.getItem('currentUser')) {
      return true;
    }
    this.alertService.error('notLoggedIn', true);
    this.router.navigate(['/login']);
    return false;
  }
}
