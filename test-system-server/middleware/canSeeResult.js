const models = require('../models');
const Result = models.Results;
const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    let token = req.headers.authorization;
    let userId;
    if (token) {
        let userToken = token.split(" ")[1];
        userId = jwt.decode(userToken).sub;
    }

    return Result.findById(req.params.id).populate('user')
        .exec()
        .then((result) => {
            if (!result) {
                return res.status(404).send('resultNotFound');
            }
            if (result.shared || !result.user || userId && result.user._id.toString() === userId.toString()) {
                return next();
            }
            return res.status(401).send('resultNotShared');
        })
        .catch((error) => {
            res.status(400).send('internalError')
        });
};