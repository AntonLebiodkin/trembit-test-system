const config = require('../config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const _ = require('lodash');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const Test = mongoose.model('Test');
const Result = mongoose.model('Results');
const SocialCredentials = mongoose.model('SocialCredentials');
const EmailCredentials = mongoose.model('EmailCredentials');
const resultService = require('./resultService');
const ObjectId = mongoose.Types.ObjectId;

const service = {};

service.authenticate = authenticate;
service.getAll = getAll;
service.getById = getById;
service.create = create;
service.update = update;
service.delete = _delete;
service.emailExists = emailExists;
service.togglePermission = togglePermission;
service.assignTest = assignTest;
service.unassignTest = unassignTest;
service.completeTest = completeTest;
service.socialLogin = socialLogin;

module.exports = service;

function authenticate(email, password) {
    return EmailCredentials.findOne({ email: email })
        .exec()
        .then((emailCredentials) => {
            if (emailCredentials && bcrypt.compareSync(password, emailCredentials.hash)) {
                return User.findOne({emailCredentials: ObjectId(emailCredentials._id)})
                    .populate('socials')
                    .populate('emailCredentials');
            } else {
                return Promise.reject('wrongUserCredentials');
            }
        })
        .then((user) => {
            if (user.emailCredentials) {
                user.emailCredentials.hash = null;
            }
            if (user.socials) {
                user.socials.forEach(social => {
                    social.uid = null;
                });
            }
            var token = jwt.sign({ sub: user._id }, config.secret);
            return Promise.resolve({user: user, token: token});
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}

function togglePermission(permission, userId) {
    return User.findById(userId)
        .exec()
        .then(user => {
            if (!user.permissions) {
                user.permissions = [];
            }
            let permissionIndex = user.permissions.indexOf(permission);
            if (permissionIndex === -1) {
                user.permissions.push(permission);
            } else {
                user.permissions.splice(permissionIndex, 1);
            }

            return user.save()
        })
        .then(user => {
            return Promise.resolve(user)
        })
        .catch(error => {
            return Promise.reject(error);
        })
}


function emailExists(email) {
    return EmailCredentials.findOne({ email: email })
        .exec()
        .then(EmailCredentials => {
            return Promise.resolve(EmailCredentials);
        })
        .catch(err => {
            return Promise.reject(err.name + ': ' + err.message);
        })
}

function getAll() {
    return User.find({})
        .populate('emailCredentials')
        .populate('socials')
        .exec()
        .then(users => {
            users.forEach((user) => {
                if (user.emailCredentials) {
                    user.emailCredentials.hash = null;
                }
                if (user.socials) {
                    user.socials.forEach(social => {
                        social.uid = null;
                    });
                }
            });
            return Promise.resolve(users);
        });
}

function getById(_id) {
    return User.findById(_id)
        .populate('assignedTests')
        .populate('emailCredentials')
        .populate('socials')
        .exec()
        .then(user => {
            if (user) {
                if (user.emailCredentials) {
                    user.emailCredentials.hash = null;
                }
                if (user.socials) {
                    user.socials.forEach(social => {
                        social.uid = null;
                    });
                }
                return Promise.resolve(user);
            } else {
                return Promise.resolve();
            }
        })
        .catch(err => {
            return Promise.reject(err.name + ': ' + err.message);
        })
}

function assignTest(userId, testId) {
    let testRef = null;
    return Test.findById(testId)
        .then(test => {
            if (test) {
                testRef = test;
                return User.findById(userId)
            } else {
                return Promise.reject('testNotFound');
            }
        })
        .then(user => {
            if (!user) return Promise.reject('noUser');
            if (!user.assignedTests) user.assignedTests = [];

            user.assignedTests.push(testRef._id);
            return user.save();
        })
        .then(user => {
            return getById(user._id);
        })
        .then(user => {
            return Promise.resolve(user);
        })
        .catch(error => {
            return Promise.reject(error);
        })
}

function unassignTest(userId, testId) {
    let testRef = null;
    return Test.findById(testId)
        .then(test => {
            if (test) {
                testRef = test;
                return User.findById(userId);
            } else {
                return Promise.reject('testNotFound');
            }
        })
        .then(user => {
            if (!user) return Promise.reject('noUser');
            if (!user.assignedTests) {
                return Promise.reject('noTestsToUnassign');
            }

            const assignIndex = user.assignedTests.indexOf(ObjectId(testId));
            if (assignIndex === -1) {
                return Promise.reject('testNotAssigned');
            }
            user.assignedTests.splice(assignIndex, 1);
            return user.save();
        })
        .then(user => {
            return getById(user._id);
        })
        .then(user => {
            return Promise.resolve(user);
        })
        .catch(error => {
            return Promise.reject(error);
        })
}

function create(emailPassword) {
    return EmailCredentials.findOne({ email: emailPassword.email })
        .exec()
        .then(emailCredential => {
            if (emailCredential) {
                return Promise.reject('emailExists');
            } else {
                return createUser();
            }
        })
        .then(user => {
            return Promise.resolve(user);
        })
        .catch(err => {
            return Promise.reject(err);
        });

    function createUser() {
        const emailCreds = new EmailCredentials();
        emailCreds.email = emailPassword.email;
        emailCreds.hash = bcrypt.hashSync(emailPassword.password, 10);
        return emailCreds.save()
            .then(emailCredentials => {
                var user = new User();

                // give admin privilegies to new user
                user.permissions = [];
                user.permissions.push('admin');

                user.emailCredentials = emailCredentials;
                return user.save();
            });
    }
}

function update(userProfile, emailCredentials) {
    return User.findById(userProfile._id)
        .populate('emailCredentials')
        .exec()
        .then(user => {
            if (!user) {
                return Promise.reject('noUser');
            }
            if (user.emailCredentials && emailCredentials) {
                if (emailCredentials.email) {
                    EmailCredentials.findOne({ email: emailCredentials.email })
                        .then(emailCredsWithChangedEmail => {
                            console.log(emailCredsWithChangedEmail);
                            if (emailCredsWithChangedEmail &&
                                emailCredsWithChangedEmail._id.toString() !== user.emailCredentials._id.toString()) {
                                return Promise.reject('emailExists');
                            }
                            user.emailCredentials.email = emailCredentials.email;
                            EmailCredentials.findOneAndUpdate({_id: user.emailCredentials._id},
                                {email: emailCredentials.email}).exec();
                        });
                }
                if (emailCredentials.password) {
                    EmailCredentials.findOneAndUpdate({_id: user.emailCredentials._id},
                        {hash: bcrypt.hashSync(emailCredentials.password, 10)}).exec();
                }
            }
            return User.findOneAndUpdate({_id: user._id}, userProfile);
        })
        .then(user => {
            return getById(user._id);
        })
        .catch(err => {
            return Promise.reject(err);
        });
}

function _delete() {
    return User.remove({ _id: _id})
        .then(() => {
            return Promise.resolve();
        })
        .catch(() => {
            return Promise.reject();
        });
}

function completeTest(result, shareResult) {
    return resultService.create(result, shareResult)
        .then(createdResult => {
            return Result.findById(createdResult._id)
                .populate('user')
                .populate('test')
                .exec()
                .then(result => {
                    //check if not one-time-test without a user
                    if (result.user) {
                        if (!result.user.completedTests) {
                            result.user.completedTests = [];
                        }
                        result.user.completedTests.push(result.test._id);
                        result.user.save();
                    }
                    return result;
                })
        })
        .then(result => {
            return Promise.resolve(result);
        })
        .catch(err => {
            return Promise.reject(err.name + ': ' + err.message);
        });
}

function socialLogin(socialUser) {
    return SocialCredentials.findOne({uid: socialUser.uid})
        .then(social => {
            if (social) {
                SocialCredentials.findOneAndUpdate({uid: socialUser.uid}, socialUser);
                return User.findOne({socials: social._id})
                    .populate('socials')
                    .populate('emailCredentials');
            } else {
                var newSocialCredentials = new SocialCredentials(socialUser);
                return newSocialCredentials
                    .save()
                    .then(social => {
                        var newUser = new User(socialUser);
                        newUser.firstName = socialUser.name;
                        newUser.lastName = '';
                        newUser.imageUrl = socialUser.image;
                        newUser.permissions = [];
                        newUser.socials = [];
                        newUser.socials.push(social);
                        return newUser.save();
                    })
                    .catch(err => {
                        return Promise.reject(err);
                    })
            }
        })
        .then(user => {
            var token = jwt.sign({ sub: user._id }, config.secret);
            if (user.emailCredentials) {
                user.emailCredentials.hash = null;
            }
            if (user.socials) {
                user.socials.forEach(social => {
                    social.uid = null;
                });
            }
            return Promise.resolve({user: user, token: token});
        })
        .catch(err => {
            return Promise.reject(err);
        });
}