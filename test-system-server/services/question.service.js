const models = require('../models');
const Question = models.Question;
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

const service = {};

service.getAll = getAll;
service.getById = getById;
service.create = create;
service.delete = _delete;
service.update = update;

module.exports = service;

function getAll() {
    return Question.find({})
        .populate('tags')
        .exec()
        .then((questions) => {
            questions.forEach(question => {
                question.question = entities.decode(question.question);
                return question;
            });
            return Promise.resolve(questions);
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}

function getById(_id) {
    return Question.findById(_id)
        .populate('tags')
        .exec()
        .then((question) => {
            if (question) {
                question.question = entities.decode(question.question);
                return Promise.resolve(question);
            }
            return Promise.reject('notFound');
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}

function create(question) {
    if (!question ||
        !question.options.length ||
        !question.correctAnswers.length) {
        return Promise.reject('wrongParameters');
    }
    if (question) {
        question.question = entities.encode(question.question);
    }
    return new Question(question)
        .save()
        .then((newQuestion) => {
            return Promise.resolve(newQuestion);
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}

function update(_id, newQuestion) {
    return Question.findByIdAndUpdate(_id, newQuestion, {new: true})
        .populate('tags')
        .exec()
        .then((question) => {
            question.question = entities.decode(question.question);
            return Promise.resolve(question)
        })
        .catch((err) => {
            return Promise.reject(err);
        })
}

function _delete(_id) {
    return Question.remove({ _id: _id})
        .then(() => {
            return Promise.resolve();
        })
        .catch((err) => {
            return Promise.reject(err);
        });
}
