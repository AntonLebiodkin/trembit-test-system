const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

const UserSchema = new Schema({
    firstName: String,
    lastName: String,
    birthDate: String,
    imageUrl: String,
    assignedTests: [{ type: ObjectId, ref: 'Test' }],
    completedTests: [{ type: ObjectId, ref: 'Test'}],
    permissions: [ String ],
    socials: [{ type: ObjectId, ref: 'SocialCredentials' }],
    emailCredentials: { type: ObjectId, ref: 'EmailCredentials' },
    language: String
});

const User = mongoose.model('User', UserSchema);
module.exports = User;