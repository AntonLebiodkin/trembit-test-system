const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmailCredentialsSchema = new Schema({
    email: String,
    hash: String
});

const EmailCredentials = mongoose.model('EmailCredentials', EmailCredentialsSchema);
module.exports = EmailCredentials;