const oneTimeTestService = require('../../services/oneTimeTest.service');

module.exports = function (req, res) {
    oneTimeTestService.setAssignee(req.params._id, req.body.assignee)
        .then((oneTimeTest) => {
            res.status(200).send(oneTimeTest);
        })
        .catch((err) => {
            var msg = 'internalError';
            switch(err) {
                case 'notFound':
                    msg = 'oneTimeTestNotFound';
                    break;
                case 'cantModifyExpired':
                    msg = 'cantModifyExpiredOneTimeTest';
                    break;
            }
            res.status(400).send(msg);
        });
};