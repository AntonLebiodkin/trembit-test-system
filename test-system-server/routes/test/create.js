const testService = require('../../services/test.service');

module.exports = function (req, res) {
    testService.create(req.body)
        .then(() => {
            res.sendStatus(200);
        })
        .catch((err) => {
            var msg = 'internalError';
            switch(err) {
                case 'noTestQuestions':
                case 'noTestDescription':
                    msg = err;
                    break;
            }
            res.status(400).send(msg);
        });
};