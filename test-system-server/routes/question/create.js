const questionService = require('../../services/question.service');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

module.exports = function (req, res) {
    if (req.body.question) {
        req.body.question = entities.encode(req.body.question);
    }

    questionService.create(req.body)
        .then((question) => {
            res.send(question);
        })
        .catch((error) => {
            var msg = "internalError";
            switch(error) {
                case 'wrongParameters':
                    msg = "wrongParameters"
                    break;
            }
            res.status(400).send(msg);
        });
};