const questionService = require('../../services/question.service');

module.exports = function (req, res) {
    questionService.getById(req.params.id)
        .then((question) => {
            res.send(question);
        })
        .catch((error) => {
            var msg = "internalError";
            switch(error) {
                case "notFound":
                    msg = "questionNotFound";
                    break;
            }
            res.status(400).send(msg);
        });
};