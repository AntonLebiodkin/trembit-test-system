const questionService = require('../../services/question.service');
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

module.exports = function (req, res) {
    if (req.body.question) {
        req.body.question = entities.encode(req.body.question);
    }

    questionService.update(req.params._id, req.body)
        .then((question) => {
            res.send(question);
        })
        .catch((err) => {
            res.status(400).send('internalError');
        });
};