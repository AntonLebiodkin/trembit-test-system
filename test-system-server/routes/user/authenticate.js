const userService = require('../../services/user.service');

module.exports = function (req, res) {
    userService.authenticate(req.body.email, req.body.password)
        .then(userWithToken => {
            if (userWithToken) {
                return res.json(userWithToken)
            } else {
                return res.status(401).send('wrongUserCredentials');
            }
        })
        .catch((err) => {
            var msg = 'internalError';
            switch(err) {
                case 'wrongUserCredentials':
                case 'noUser':
                    msg = err;
                    break;
            }
            res.status(400).send(msg);
        });
};