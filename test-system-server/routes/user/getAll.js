const userService = require('../../services/user.service');

module.exports = function (req, res) {
    userService.getAll()
        .then(tests => {
            res.send(tests);
        })
        .catch((err) => {
            var msg = 'internalError';
            res.status(400).send(msg);
        });
};