const userService = require('../../services/user.service');

module.exports = function (req, res) {
    userService.update(req.body.user, req.body.emailCredentials)
        .then(user => {
            res.send(user);
        })
        .catch((err) => {
            var msg = 'internalError';
            switch(err) {
                case 'noUser':
                case 'emailExists':
                    msg = err;
                    break;
            }
            res.status(400).send(msg);
        });
};