const userService = require('../../services/user.service');

module.exports = function (req, res) {
    userService.unassignTest(req.body.userId, req.body.testId)
        .then((user) => {
            res.send(user);
        })
        .catch((err) => {
            var msg = 'internalError';
            switch(err) {
                case 'testNotFound':
                case 'noUser':
                case 'noTestsToUnassign':
                case 'testNotAssigned':
                    msg = err;
                    break;
            }
            res.status(400).send(msg);
        });
};