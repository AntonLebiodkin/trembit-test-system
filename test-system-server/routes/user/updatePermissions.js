const userService = require('../../services/user.service');

module.exports = function (req, res) {
    let token = req.headers.authorization;
    if (!token) return res.sendStatus(401);

    let permission = req.body.permission;
    let userToChange = req.body.userId;

    if (!permission) return res.sendStatus(400);

    userService.togglePermission(permission, userToChange)
        .then(user => {
            if (user) {
                res.send(user);
            } else {
                res.sendStatus(404);
            }
        })
        .catch((err) => {
            var msg = 'internalError';
            res.status(400).send(msg);
        });
};