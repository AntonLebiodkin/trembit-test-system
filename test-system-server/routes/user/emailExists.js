const userService = require('../../services/user.service');

module.exports = function (req, res) {
    userService.emailExists(req.query.email)
        .then(EmailCredentials => {
            if (EmailCredentials) {
                return res.json({ exists: true})
            } else {
                return res.json({ exists: false});
            }
        })
        .catch((err) => {
            var msg = 'internalError';
            res.status(400).send(msg);
        });
};